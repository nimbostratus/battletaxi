#include <stdlib.h>
#include <confuse.h>
#include <assert.h>
#include <string.h>
#include <limits.h>
#include "dirs.h"
#include "includes.h"


WeaponFactory *weaponInit (char *apath)
{
	assert (*apath);
	
	WeaponFactory *ret = (WeaponFactory *) malloc (sizeof(WeaponFactory));
	int i;
	int ttype;
	char *tname = NULL;
	char *ticonimage = NULL;
	char *tshotimage = NULL;
	double tspeed;
	int tdamage;
	double trange;
	char thisfile[NAME_MAX+1];
	int parseret;
	
	cfg_opt_t opts[] = {
		CFG_SIMPLE_INT("type", &ttype),
		CFG_SIMPLE_STR("name", &tname),
		CFG_SIMPLE_STR("shotimage", &tshotimage),
		CFG_SIMPLE_STR("iconimage", &ticonimage),
		CFG_SIMPLE_FLOAT("speed", &tspeed),
		CFG_SIMPLE_INT("damage", &tdamage),
		CFG_SIMPLE_FLOAT("range", &trange),
		CFG_END()
	};
	cfg_t *cfg;
	
	char **weaponfiles;
	
	weaponfiles = DirGetFiles (apath, ".conf");
	
	ret->weaponcount=0;
	ret->weapons = (Weapon**) malloc (sizeof (Weapon*) +1);

	cfg = cfg_init(opts, 0);
		
	for (i=0; weaponfiles[i] != NULL; ++i)
	{
		sprintf (thisfile, "%s/%s", apath, weaponfiles[i]);
		parseret = cfg_parse(cfg, thisfile);

		ret->weapons = realloc (ret->weapons, sizeof(Weapon*) * (ret->weaponcount + 2 ));
		ret->weaponcount++;
		
		ret->weapons[ret->weaponcount-1] = (Weapon*) malloc (sizeof (Weapon));
		ret->weapons[ret->weaponcount-1]->type = ttype;
		ret->weapons[ret->weaponcount-1]->name = (char*) malloc (strlen (tname)+1);
		strcpy (ret->weapons[ret->weaponcount-1]->name, tname);
		ret->weapons[ret->weaponcount-1]->speed = tspeed;
		ret->weapons[ret->weaponcount-1]->damage = tdamage;
		ret->weapons[ret->weaponcount-1]->range = trange;

		sprintf (thisfile, "%s/%s", apath, tshotimage);
		ret->weapons[ret->weaponcount-1]->shotimage = imageloadalpha (thisfile);
		sprintf (thisfile, "%s/%s", apath, ticonimage);
		ret->weapons[ret->weaponcount-1]->iconimage = imageloadalpha (thisfile);
				
	}
	cfg_free(cfg);

        ret->shots=(Shot *)malloc(sizeof(Shot));
        ret->shots->next=NULL;
        ret->shots->x=0;
        ret->shots->y=0;
        ret->shots->dx=0;
	ret->shots->dy=0;
	
	ret->weapons[ret->weaponcount] = NULL;
	
	return ret;
}

void weaponNewShot(WeaponFactory *aweapons, int x, int y, int delta, int whichweapon)
{
	Shot *runner=aweapons->shots;
	while(runner->next!=NULL) runner=runner->next;
	
	runner->x=x;
	runner->y=y;
	runner->weapon = aweapons->weapons[whichweapon];
	if (runner->weapon->type == WEAPON_TYPE_LASER)
		runner->dx=delta;
	else if (runner->weapon->type == WEAPON_TYPE_BOMB)
		runner->dy=delta;
	runner->next=(Shot*)malloc(sizeof(Shot));
	runner->next->next=NULL;
}

void weaponDeleteShots(WeaponFactory *aweapons)
{
	Shot *runner=aweapons->shots, *tmp;
	
	while(runner->next!=NULL){
		tmp=runner->next;
		free(runner);
		runner=tmp;
	}
	aweapons->shots=runner;
}

void weaponDraw (SDL_Surface *ascreen, WeaponFactory *aweapons)
{
	Shot *runner=aweapons->shots;

	while(runner->next!=NULL){
		weaponDrawShot(ascreen, aweapons, runner);
		runner=runner->next;
	}
}

void weaponExit(WeaponFactory *aweapons)
{
	int i;
	
	for(i=0;i<aweapons->weaponcount;i++)
	{
		SDL_FreeSurface(aweapons->weapons[i]->shotimage);
		SDL_FreeSurface(aweapons->weapons[i]->iconimage);
		free (aweapons->weapons[i]->name);
		free (aweapons->weapons[i]);
		aweapons->weapons[i] = NULL;
	}
	free(aweapons);
}

// internal functions

void weaponDrawShot(SDL_Surface *ascreen, WeaponFactory *aweapons, Shot *ashot)
{
	SDL_Rect rcsrc, rcdst;
	
	rcsrc.x=0; 		rcsrc.y=0;
	rcdst.x=ashot->x; 	rcdst.y=ashot->y;
	rcdst.w=rcsrc.w=ashot->weapon->shotimage->w;
	rcdst.h=rcsrc.h=ashot->weapon->shotimage->h;
	
	SDL_BlitSurface(ashot->weapon->shotimage, &rcsrc, ascreen, &rcdst);
}

