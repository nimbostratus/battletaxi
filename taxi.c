// #include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include "includes.h"


TaxiFactory *taxiInit (char *fnamestart, SDL_Surface *colmap)
{
	int i;
	char *fname=(char*)malloc(strlen(fnamestart)+10);
	TaxiFactory *ret=(TaxiFactory *)malloc(sizeof(TaxiFactory));
	
	for(i=0;i<IDX_COUNT;i++){
		sprintf(fname,"%s-%d.png",fnamestart,i);
		ret->images[i]=imageloadalpha(fname);
	}
	free(fname);
	
	ret->dx=0;
	ret->dy=0;
	ret->weapon=0;
	ret->status=TAXI_STAT_DIRLEFT;
	initXyOnLevel(ret, colmap);

	return ret;		
}

void taxiDraw (SDL_Surface *ascreen, TaxiFactory *ataxi, Uint8 *keystroke, SDL_Surface *colmap)
{
	// draw passenger
	if(ataxi->status&TAXI_STAT_HASPASSENGER){
		if(ataxi->status&TAXI_STAT_DIRLEFT)
			taxiDrawFrame(ascreen, ataxi, IDX_PASSENGERLEFT);
		else
			taxiDrawFrame(ascreen, ataxi, IDX_PASSENGERRIGHT);
	}
	// gear in not crashed
	if(!(ataxi->status&TAXI_STAT_GEAROUT)&&!(ataxi->status&TAXI_STAT_CRASHED)){
		if(ataxi->status&TAXI_STAT_DIRLEFT)
			taxiDrawFrame(ascreen, ataxi, IDX_TOLEFTCOLLAPSED);
		else
			taxiDrawFrame(ascreen, ataxi, IDX_TORIGHTCOLLAPSED);
	}
	// gear out not crashed
	if(ataxi->status&TAXI_STAT_GEAROUT&&!(ataxi->status&TAXI_STAT_CRASHED)){
		if(ataxi->status&TAXI_STAT_DIRLEFT)
			taxiDrawFrame(ascreen, ataxi, IDX_TOLEFTEXPANDED);
		else
			taxiDrawFrame(ascreen, ataxi, IDX_TORIGHTEXPANDED);
	}
	
	// crashed
	if(ataxi->status&TAXI_STAT_CRASHED){
		ataxi->dx=0;
		ataxi->dy=0;
		if(ataxi->status&TAXI_STAT_DIRLEFT)
			taxiDrawFrame(ascreen, ataxi, IDX_CRASHTOLEFT);
		else
			taxiDrawFrame(ascreen, ataxi, IDX_CRASHTORIGHT);
		return;
	}
	
	if(keystroke[SDLK_UP])
		taxiDrawFrame(ascreen, ataxi, IDX_FIRESOUTH);	
	
	if(keystroke[SDLK_LEFT]&&!(ataxi->status&TAXI_STAT_GEAROUT))
		taxiDrawFrame(ascreen, ataxi, IDX_FIREEAST);
	
	if(keystroke[SDLK_DOWN]){
		if(ataxi->status&TAXI_STAT_DIRLEFT)
			taxiDrawFrame(ascreen, ataxi, IDX_FIRENORTHTOLEFT);
		else
			taxiDrawFrame(ascreen, ataxi, IDX_FIRENORTHTORIGHT);
	}
	if(keystroke[SDLK_RIGHT]&&!(ataxi->status&TAXI_STAT_GEAROUT))
		taxiDrawFrame(ascreen, ataxi, IDX_FIREWEST);
}

void taxiExit(TaxiFactory *ataxi)
{
	int i;
	
	for(i=0;i<IDX_COUNT;i++)
		SDL_FreeSurface(ataxi->images[i]);
	free(ataxi);
}

void taxiSetAlive (TaxiFactory *ataxi, SDL_Surface *colmap)
{
	ataxi->dx=0;
	ataxi->dy=0;
	ataxi->status=TAXI_STAT_DIRLEFT;
	initXyOnLevel(ataxi, colmap);
}

void taxiDrawFrame(SDL_Surface *ascreen, TaxiFactory *ataxi, int whichimg)
{
	SDL_Rect rcsrc, rcdst;
	
	rcsrc.x=0; 		rcsrc.y=0;
	rcdst.x=ataxi->x; 	rcdst.y=ataxi->y;
	rcdst.w=rcsrc.w=ataxi->images[whichimg]->w;
	rcdst.h=rcsrc.h=ataxi->images[whichimg]->h;
	
	SDL_BlitSurface(ataxi->images[whichimg], &rcsrc, ascreen, &rcdst);
}

void initXyOnLevel(TaxiFactory *ataxi, SDL_Surface *colmap)
{
	Point p;
	
	if(!drawGetFirstColor(&p, colmap, 0x00ff01)){
		DEBUG("Taxi Start Point not found!");
		return;
	}
	
	ataxi->x=p.x;
	ataxi->y=p.y;
}
