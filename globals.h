#ifndef GLOBALS_H
#define GLOBALS_H

typedef struct S_POINT{
	int x;
	int y;
}Point;

typedef struct S_DOUBLEPOINT{
	double x;
	double y;
}DoublePoint;

#define MAXX 1024
#define MAXY 768
#define BPP 32

#define MAXSCORES 10	// number of highscores to save

#define LANDSPEED 0.17	// defines how fast you may be when landing so you dont crash
#define SPEED 0.000466	// how fast you speed up when pressing keys * ticks
#define SLOWDOWN 0.00003	// slowdown rate of taxi
#define FIRERATE 413	// how fast we fire
//#define STARTSPEED -0.12 // speed when auto starting taxi from plattform
//#define STARTSPEED 0 // config file
#define MANSPEED -0.04	// speed of walking man

#define HIGHSCORENAME	"highscore.dat"
#define true  1
#define false 0

#ifndef NDEBUG

#define DEBUG(t) {printf ("DEBUG: (in %s at %d) :  %s\n",__FILE__, __LINE__, t); fflush (stdout);}
#define DEBUGi(t) {printf ("DEBUG: (in %s at %d) :  %i\n",__FILE__, __LINE__, t); fflush (stdout);}
#define DEBUGp(t) {printf ("DEBUG: (in %s at %d) :  %p\n",__FILE__, __LINE__, t); fflush (stdout);}
#define DEBUGf(t) {printf ("DEBUG: (in %s at %d) :  %f\n",__FILE__, __LINE__, t); fflush (stdout);}
#define DEBUGx(t) {printf ("DEBUG: (in %s at %d) :  %x\n",__FILE__, __LINE__, t); fflush (stdout);}

#else

#define DEBUG(t) {}
#define DEBUGi(t) {}
#define DEBUGp(t) {}
#define DEBUGf(t) {}

#endif

#endif
