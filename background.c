#include "includes.h"
#include "background.h"
#include <malloc.h>
#include <string.h>
#include <assert.h>
#include "voxel.h"
#include "starfield.h"
#include "sky.h"


BackgroundFactory *backgroundInit ()
{
	BackgroundFactory *ret = (BackgroundFactory*) malloc (sizeof (BackgroundFactory));
	assert (ret);
	
	ret->count = 0;
	ret->backgrounds = (Background**) malloc (sizeof (Background*)); // will be realloced in bgregister
	ret->backgrounds[ret->count] = NULL;
	
	// static register of bg effects. May be replayed by dl* some day
	
	bgregister (ret, "starfield", (void*)starfieldInit, (void*)starfieldDraw, (void*)starfieldExit);
	bgregister (ret, "voxel", (void*)voxelInit, (void*)voxelDraw, (void*)voxelExit);
	bgregister (ret, "sky", (void*)skyInit, (void*)skyDraw, (void*)skyExit);
	
	return ret;
}

void backgroundDraw (SDL_Surface *asurface, BackgroundFactory *abackground)
{
	assert (asurface);
	assert (abackground);
	
	int i;
	
	for (i=0; i<abackground->count; ++i)
		if (abackground->backgrounds[i]->active)
			abackground->backgrounds[i]->draw(asurface, abackground->backgrounds[i]->factory);
}

void backgroundExit(BackgroundFactory *abackground)
{
	assert (abackground);
	
	int i;
	
	for (i=0; i<abackground->count; ++i)
		abackground->backgrounds[i]->exit(abackground->backgrounds[i]->factory);

	free (abackground);
	abackground = NULL;
}

int backgroundSetActive (BackgroundFactory *abackground, char *aname, int astate)
{
	assert (abackground);
	assert (aname);
	assert (astate == 1 || astate == 0);
	Background *tmp = getBackgroundByName (abackground, aname);

	if (tmp != NULL)
	{
		tmp->active = astate;
		return 1;
	} else {
		return 0;
	}
}
// internal functions

void bgregister (BackgroundFactory *afactory, char *aname, backgroundinit_t ainit, backgrounddraw_t adraw, backgroundexit_t aexit)
{
	assert (afactory);
	assert (aname);
	assert (ainit);
	assert (adraw);
	assert (aexit);
	
	Background *tmp = (Background*) malloc (sizeof (Background));
	assert (tmp);
	
	tmp->active = 0;
	tmp->init = ainit;
	tmp->draw = adraw;
	tmp->exit = aexit;
	
	tmp->factory = ainit();
	
	tmp->name = (char*) malloc (strlen(aname) + 1);
	strcpy (tmp->name, aname);
	
	afactory->count++;
	afactory->backgrounds = (Background**) realloc (afactory->backgrounds, ( sizeof (Background*) * (afactory->count + 1)));
	
	afactory->backgrounds[afactory->count-1] = tmp;
	afactory->backgrounds[afactory->count] = NULL;
}

Background *getBackgroundByName (BackgroundFactory *afactory, char *aname)
{
	assert (afactory);
	assert (aname);
	

	int i;

	for (i=0; i<afactory->count; ++i)
		if (strcmp(afactory->backgrounds[i]->name, aname) == 0)
			return afactory->backgrounds[i];
		
	
	return NULL;
}

void backgroundDisableAll (BackgroundFactory *afactory)
{
	assert (afactory);
	
	int i;
	
	for (i=0; i<afactory->count; ++i)
		afactory->backgrounds[i]->active = 0;

}
