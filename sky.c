#include <stdlib.h>
#include <assert.h>
#include "sky.h"
#include "includes.h"


SkyFactory *skyInit ()
{
	SkyFactory *ret = ( SkyFactory * ) malloc( sizeof(SkyFactory) );
	assert(ret);

	ret->background = imageloadalpha ("images/skybackground.png");
	
	return ret;
	
}

void skyDraw (SDL_Surface *asurface, SkyFactory *asky)
{
	assert (asurface);
	assert (asky);
	assert (asky->background);
	SDL_Rect rcsrc, rcdst;

	rcsrc.x = rcsrc.y = rcdst.x = rcdst.y = 0;
	rcsrc.w = rcdst.w = asky->background->w;
	rcsrc.h = rcdst.h = asky->background->h;
	SDL_BlitSurface (asky->background, &rcsrc, asurface, &rcdst);
	
}

void skyExit(SkyFactory *asky)
{
	SDL_FreeSurface (asky->background);
	asky->background = NULL;
	free (asky);
	asky = NULL;
}
