#ifndef HIGHSCORE_H
#define HIGHSCORE_H

typedef struct S_SCORE{
	int score;
	unsigned int date;
	int level;
	char name[30];
}Score;

typedef struct S_HIGHSCOREFACTORY{
	Score scores[MAXSCORES];
}HighscoreFactory;

HighscoreFactory *highscoreInit ();
void highscoreInsNewScore(HighscoreFactory *ahighscore, int score, char *name, int level);
int highscoreIsNew(HighscoreFactory *ahighscore, int score);
int highscoreGetRank(HighscoreFactory *ahighscore, int score);
void highscoreDraw (SDL_Surface *asurface, HighscoreFactory *ahighscore, FontFactory *fontfac);
void highscoreExit(HighscoreFactory *ahighscore);

#endif
