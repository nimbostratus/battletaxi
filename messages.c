#include <assert.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "includes.h"


MessageFactory *internalfac=NULL;

MessageFactory *messageInit (int amessageticks, FontFactory *aff)
{
	assert (aff);
	MessageFactory *ret;
	ret = (MessageFactory*)malloc(sizeof(MessageFactory));
	ret->visible=0;
	ret->ff=aff;
	ret->text=NULL;
	ret->enddisplay=SDL_GetTicks() + 500;
	
	internalfac = ret;
	return ret;
}
void messageExit (MessageFactory *afactory)
{
	assert (afactory);
	if (afactory->text != NULL)
		free (afactory->text);
	free (afactory);
	afactory=NULL;
	internalfac = NULL;
}

void messageDraw (SDL_Surface *ascreen, MessageFactory *afactory)
{
	if (afactory == NULL)
		afactory = internalfac;
	
	if (!afactory->visible) return;
	
	if(SDL_GetTicks()>afactory->enddisplay)
	afactory->visible=false;
	
	bmpFontDrawStrC (ascreen, afactory->ff, 0, ascreen->w/2, ascreen->h/2, afactory->text);
}

void messageThrow (char *amessage)
{
	assert (internalfac);
	internalfac->text = (char*)realloc(internalfac->text, strlen(amessage)+1);
	strcpy (internalfac->text, amessage);
	internalfac->visible=1;
	internalfac->enddisplay = SDL_GetTicks()+1000;
}


