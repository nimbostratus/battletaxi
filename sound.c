#include <assert.h>
#include <SDL_mixer.h>
#include "includes.h"

Mix_Music *sndMusic=NULL;
int useaudio = 1;

void sndInit (void)
{
	int audio_channels = 2;
	int audio_rate = 22050;
	int audio_buffers = 4096;
	Uint16 audio_format = AUDIO_S16;
	int audio_volume = MIX_MAX_VOLUME;
	
	if (!SDL_InitSubSystem(SDL_INIT_AUDIO))
	{
		fprintf (stderr, "sndInit: couldn't init Audio. Sounds are not available (%s)\n", SDL_GetError());
		useaudio = 0;
		return;
	}
	
/* Open the audio device */
        if (Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) < 0) {
               fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
	       useaudio = 0;
        } else {
                Mix_QuerySpec(&audio_rate, &audio_format, &audio_channels);
                printf("Opened audio at %d Hz %d bit %s, %d bytes audio buffer\n", audio_rate, (audio_format&0xFF), (audio_channels > 1) ? "stereo" : "mono", audio_buffers );
        }
	useaudio = 1;

	Mix_VolumeMusic (audio_volume);
//	Mix_SetMusicCMD(getenv("MUSIC_CMD"));
}

void sndExit (void)
{
	if (!useaudio) return;
	
	if (sndMusic != NULL)
	{
		Mix_FreeMusic( sndMusic );
		sndMusic = NULL;
	}
	Mix_CloseAudio();
}

void sndStartBackgroundMusic (char *afile)
{
	int audio_looping = 0;
	if (!useaudio) return;
	
	assert (afile);
	if (sndMusic != NULL)
	{
		Mix_FadeOutMusic ( 1500 );
		Mix_FreeMusic( sndMusic );
	}
		
	sndMusic = Mix_LoadMUS(afile);
	if (sndMusic == NULL)
	{
		fprintf (stderr, "sndStartBackgroundMusic: couldn't load %s : %s", afile, SDL_GetError() );
		useaudio = 0;
		return;
	}
//	Mix_PlayMusic (sndMusic, 0);
	Mix_FadeInMusic (sndMusic, audio_looping, 20000);
	
}

void sndMuteBackgroundMusic (void)
{
	if (!useaudio) return;
	return;
}


