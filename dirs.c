#include "dirs.h"
#include <dirent.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "globals.h"

char **DirGetFiles (char *adir, char *aext)
{
	assert (adir);
	assert (aext);

	DIR *dirstream;
	struct dirent *tdirent;
	int n=0;
	char **ret;
	char *tmpstr;

	dirstream = opendir (adir);
	ret = NULL;
	
	if (!dirstream)
		perror ("opendir");
	
	while ((tdirent = readdir (dirstream)) != NULL)
	{
		tmpstr = strstr (tdirent->d_name, ".conf");
		if (( tmpstr != NULL) && (strlen(tmpstr) == 5))
		{
			ret = (char **) realloc (ret, sizeof (char *) * (n + 2));
			ret[n] = (char *) malloc (NAME_MAX + 1);
			strcpy (ret[n], tdirent->d_name);
			n++;
		}
	}
	free (dirstream);
	ret[n] = NULL;
	
	return ret;
		
}
