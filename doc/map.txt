How to create battletaxi maps
=============================


* Battletaxi maps at a glance

  A Battletaxi map constists of two png files with a resolution 
  of 1024x760 pixels. One image will be displayed "as is" to
  the player, the other is a collision map for the game, which
  contains some more information.

* The display map

  The file named map?-1.png (where ? is the map number) should be
  a 32bit 1024x768 png graphic with alpha channel. It is the 
  map displayed to the player. Every area which is not alpha will
  be drawn. Everything else will not be drawn. Easy, mh?

* The collision map

  This piece must also be a 32bit 1024x768 png. In here, we
  differ some color values to control the game :

  RGB Value             Meaning

  Alpha FF + no color   Free Space to travel through
  0000FF                Solid. The players cabs will crash here
  FFxx00                Landing position number xx. Start with 01, 
                        and go on. "The man" will appear randomly
                        at the bases. Make a thin horizontal line
                        of that color.
  00FFxx                Cab start positions. Paint some 1x1 pixel
                        points of that color (with increasing xx)
                        onto the map (not into 0000FF or FFxx00
                        please!). The cabs will start at a 
                        random position of these.

  This image should be named map?-0.png

* Placing the maps

  If your done so far, put the maps images (remember - map?-1.png
  and map?-0.png) in the images/ directory. Battletaxi will 
  find them and load them. Due to reuse of the source, please 
  put the xcf (if you use gimp) named map?.xcf besides them.


