#ifndef WEAPONS_H
#define WEAPONS_H

typedef enum {
	WEAPON_TYPE_LASER=0,		// Weapons that fly straight forward
	WEAPON_TYPE_BAL=1,		// ballistic weapons
	WEAPON_TYPE_STAT=2,		// stationary weapons like mines
	WEAPON_TYPE_BOMB=3,		// Bombs falling vertical
	WEAPON_TYPE_TOOL=4		// Tools
}WeaponType;

typedef struct S_WEAPON {
	int		type;
	char		*name;
	SDL_Surface 	*shotimage;
	SDL_Surface 	*iconimage;
	double		speed;
	int		damage;
	double		range;
}Weapon;

typedef struct S_SHOT{
	Weapon *weapon;
	double x;
	double y;
	double dx;
	double dy;
	struct S_SHOT *next;
}Shot;

typedef struct S_WEAPONFACTORY{
	Shot *shots;
	Weapon **weapons;
	int weaponcount;
}WeaponFactory;

WeaponFactory *weaponInit (char *apath);
/*
  Inits the Factory. The Directory $data/apath/ will be searched for 
  *.conf files. Every file defines one weapon and tells us, what image
  should be read
*/

void weaponNewShot(WeaponFactory *aweapons, int x, int y, int delta, int whichweapon);
void weaponDeleteShots(WeaponFactory *aweapons);
void weaponDraw(SDL_Surface *ascreen, WeaponFactory *aweapons);
void weaponExit(WeaponFactory *aweapons);

// internal functions
void weaponDrawShot(SDL_Surface *ascreen, WeaponFactory *aweapons, Shot *ashot);

#endif
