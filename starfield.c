#include <stdlib.h>
#include <assert.h>
#include "starfield.h"
#include "includes.h"


StarfieldFactory *starfieldInit ()
{
	StarfieldFactory *ret = ( StarfieldFactory * ) malloc( sizeof(StarfieldFactory) );
	assert(ret);
	
	int i;

	ret->stars=(Star**)malloc(sizeof(Star*)*(MAXSTARS+1));

	for (i=0 ;i<MAXSTARS; i++){
		ret->stars[i]=(Star *)malloc(sizeof(Star));
		starInit(ret->stars[i]);		
	}
	ret->stars[MAXSTARS]=NULL;

	return ret;
	
}

void starfieldDraw (SDL_Surface *asurface, StarfieldFactory *astarfield)
{
	assert (asurface);
	assert (astarfield);
	int i;

	for (i=0; astarfield->stars[i]!=NULL; i++){
 		starDraw(asurface, astarfield->stars[i]);
		starMove(astarfield->stars[i]);
	}
}

void starfieldExit(StarfieldFactory *astarfield)
{
	int i;
	for (i=0; astarfield->stars[i]!=NULL; i++)
		free(astarfield->stars[i]);
	free (astarfield->stars);
	free (astarfield);
}



//-----------------------internal functions

void starInit(Star *s)
{
	double midx=MAXX/2.0, midy=MAXY/2.0, speed;
	
	s->x=rand()%MAXX;
	s->y=rand()%MAXY;
	
	speed=(rand()%2000)/500.0+0.01;
	s->dx=(abs(midx-s->x)/midx)*speed;
	s->dy=(abs(midy-s->y)/midy)*speed;
	s->traillength=1;
	
	if(s->x<MAXX/2) s->dx*=-1;
	if(s->y<MAXY/2) s->dy*=-1;
	
	s->grayscale=(rand()%236)+20;
}

void starMove(Star *s)
{
	s->traillength+=0.02;
	s->x+=s->dx;
	s->y+=s->dy;
	s->dx*=1.02;
	s->dy*=1.02;
}

void starDraw(SDL_Surface *ascreen, Star *s)
{	
	int i;
	assert(ascreen);assert(s);
	
	for(i=0;i<(int)s->traillength;i++)
		if(!drawpixel(ascreen, s->x+s->dx*i, s->y+s->dy*i, s->grayscale, s->grayscale, s->grayscale)){
			starInit(s);
			return;
		}
}
