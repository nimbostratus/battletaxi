
#ifndef SKY_H
#define SKY_H

#include <SDL.h>


typedef struct S_SKYFACTORY{
	SDL_Surface *background;
}SkyFactory;

SkyFactory *skyInit ();
void skyDraw (SDL_Surface *asurface, SkyFactory *asky);
void skyExit(SkyFactory *asky);

#endif
