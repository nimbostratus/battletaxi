#define MAPWIDTH  512
#define MAPHEIGHT 512
#define VOXSIZE   4

#include <SDL.h>
/*-----------------------------------------------------------------------------*/
typedef struct _VOXELFACT{
  double viewX, viewY, viewAngle;
  double viewAngleDir, viewMoveDir;
  
  int waterHeight;
  double waterHeightAngle;
}VOXELFACT, *LPVOXELFACT;
/*-----------------------------------------------------------------------------*/
extern LPVOXELFACT            GlpVoxFact;
extern SDL_Surface *          ddHeightMap;
/*-----------------------------------------------------------------------------*/
int voxelInit();
void voxelExit();
void voxelDraw(SDL_Surface *ascreen);
void voxelSetPositions();
int DDDrawVoxel(SDL_Surface *ascreen, int maxheight, int height, unsigned int color, int line, int step);

