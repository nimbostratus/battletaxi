#ifndef SOUND_H
#define SOUND_H

void sndInit (void);
void sndExit (void);

void sndStartBackgroundMusic (char *afile);
void sndMuteBackgroundMusic (void);


#endif
