
#ifndef STARFIELD_H
#define STARFIELD_H

#include <SDL.h>

#define MAXSTARS 1000

typedef struct S_STAR{
	double x,y;
	double dx, dy;
	double traillength;
	int grayscale;
}Star;

typedef struct S_STARFIELDFACTORY{
	Star **stars;
}StarfieldFactory;

StarfieldFactory *starfieldInit ();
void starfieldDraw (SDL_Surface *asurface, StarfieldFactory *astarfield);
void starfieldExit(StarfieldFactory *astarfield);

// internal functions

void starInit(Star *s);
void starMove(Star *s);
void starDraw(SDL_Surface *ascreen, Star *s);


#endif
