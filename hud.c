#include <assert.h>
#include <malloc.h>
#include "includes.h"


HudFactory *hudInit (char *aimage)
{
	assert (aimage);
	
	HudFactory *ret;
	
	ret = (HudFactory*) malloc (sizeof (HudFactory));
	ret->surface = imageloadalpha(aimage);
	ret->background = imageloadalpha(aimage);
	
	ret->alpha = 0;
	ret->visible = 1;
	ret->score = 0;
	ret->cash = 0;
	ret->frags = 0;
	ret->ranking = 0;
	ret->fuel = 0;
	ret->fading = 0;
	ret->y = 0;
	
	return ret;
}

void hudReset(HudFactory *ahud)
{
	ahud->score=0;
}

void hudExit (HudFactory *ahud)
{
	assert (ahud);
	SDL_FreeSurface (ahud->surface);
	free (ahud);
	ahud = NULL;
}

void hudDraw(SDL_Surface *ascreen, FontFactory *afont, int afontnum, HudFactory *ahud)
{
	assert (ahud);
	assert (ascreen);
	
	SDL_Rect rcsrc, rcdest;
	char *tmpstr = (char *) malloc (120);
	
	rcdest.x=rcdest.y = rcsrc.x=rcsrc.y=0;
	rcdest.h = rcsrc.h=ahud->surface->h;
	rcdest.w = rcsrc.w=ahud->surface->w;

		sprintf (tmpstr, "SCORE: %d | CASH: %d | FUEL: %d | FRAGS: %d", ahud->score, ahud->cash, ahud->fuel, ahud->frags);
	
		SDL_BlitSurface (ahud->background, &rcsrc, ahud->surface, &rcdest);

	if (ahud->fading == -1)
	{
		if (ahud->y < -(ahud->surface->h))
		{
			ahud->fading = 0;
			ahud->visible = 0;
		} else {
			rcdest.y = ahud->y;
			ahud->y -= 3;
		}
	}
	else if (ahud->fading == 1)
	{
		if (ahud->y > -1)
		{
			ahud->fading = 0;
		} else {
			ahud->visible = 1;
			rcdest.y = ahud->y;
			ahud->y += 3;
		}
	}
			
	if (ahud->visible)
	{
		bmpFontDrawStrL (ahud->surface, afont, afontnum, 5, 5, tmpstr);
		SDL_BlitSurface (ahud->surface, &rcsrc, ascreen, &rcdest);
	}
	free (tmpstr);
}

void hudShow(HudFactory *ahud)
{
	assert (ahud);
	if(SDL_GetTicks() - ahud->laststatechange > 250)
	{
		ahud->visible = 1;
		ahud->fading = 1;
		ahud->laststatechange = SDL_GetTicks();
	}
}

void hudHide(HudFactory *ahud)
{
	assert (ahud);
	if(SDL_GetTicks() - ahud->laststatechange > 250)
	{
		ahud->visible = 0;
		ahud->fading = -1;
		ahud->laststatechange = SDL_GetTicks();
	}
}

void hudToggle(HudFactory *ahud)
{
	assert (ahud);
	if(SDL_GetTicks() - ahud->laststatechange > 250)
	{
		if (ahud->visible)
		{
			ahud->fading = -1;
		} else {
			ahud->fading = 1;
		}
		ahud->laststatechange = SDL_GetTicks();
	}
}

void hudSetScore (HudFactory *ahud, int ascore)
{
	assert (ahud);
	ahud->score = ascore;
}

void hudAddScore (HudFactory *ahud, int aadd)
{
	assert (ahud);
	ahud->score += aadd;
}
