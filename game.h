#ifndef GAME_H
#define GAME_H

typedef struct S_GAME{
	SDL_Surface *screen;
	BackgroundFactory *bg;
	TaxiFactory *taxi;
	FontFactory *fontfac;
	LevelFactory *levels;
	HudFactory *hud;
	MenuFactory *mainmenu;	
	WeaponFactory *weapons;
	MessageFactory *messages;
	Man *man;
	HighscoreFactory *highscore;
	InputFactory *input;
 	unsigned int starttime, ticks, nextshot, nextescape, showhighscoretillticks;
	int framecounter;
	int pause;
	int menuactive;
	int highscoreactive;
	
	int lastkeyticks;
	double fps;
	Uint8 *keystroke;
	char quit;
}Game;

// update stuff - main game-operations!
void gameUpdate (Game *g);

void gameUpdateWeapon(Game *g);
int  gameUpdateWeaponDoesCollied(Game *g, Shot *s);
int  gameUpdateWeaponHitsPassenger(Game *g, Shot *s);

void gameUpdateTaxi(Game *g, int ticks);
void gameUpdateTaxiKeyboard(Game *g, int ticks);
void gameUpdateTaxiCheckCrashed(TaxiFactory *ataxi, SDL_Surface *colmap);

void gameUpdateKeyboard(Game *g);
void gameUpdateMan(Game *g, int ticks);
void gameUpdateManLanded(Game *g);

// init and exit ...
Game *gameInit ();
void gameExit(Game *g);

// internal stuff
void gameInitMenu(Game *g);

// callback functions
void menuCallbNew (Menu *amenu, void *adata);
void menuCallbConnect (Menu *amenu, void *adata);
void menuCallbQuit (Menu *amenu, void *adata);
void menuCallbLevel (Menu *amenu, void *adata);
void menuCallbHighscore (Menu *amenu, void *adata);
void menuCallbFull (Menu *amenu, void *adata);

void inputCallbName(void *vgame, char *ainput);
void inputCallbHost(void *vgame, char *ainput);


#endif
