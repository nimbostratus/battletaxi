#include <assert.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "includes.h"

Bubble *bubbleInit (FontFactory *ff)
{
	Bubble *ret=(Bubble*)malloc(sizeof(Bubble));
	
	ret->x=0;
	ret->y=0;
	ret->enddisplay=0;
	ret->visible=false;
	ret->text=NULL;
	ret->textwidth=0;
	ret->ff=ff;
	
	return ret;
}

void bubbleStart(Bubble *abubble, int ax, int ay, char *name, int displaytime)
{
	free(abubble->text);
	
	abubble->x=abubble->bubblex=ax;
	abubble->y=abubble->bubbley=ay;
	abubble->enddisplay=SDL_GetTicks()+displaytime;
	abubble->visible=true;
	
	abubble->text=malloc(strlen(name)+1);
	strcpy(abubble->text, name);
	abubble->textwidth=bmpFontTextWidth (abubble->ff, 1, abubble->text);
	
	if(abubble->x+abubble->textwidth/2 > MAXX-40)
		abubble->bubblex=MAXX-abubble->textwidth/2-40;
	if(abubble->x-abubble->textwidth/2 < 10)
		abubble->bubblex=abubble->textwidth/2+10;
	if(abubble->y<100)
		abubble->bubbley=200;
}

void bubbleDraw(Bubble *abubble, SDL_Surface *ascreen)
{
	if(!abubble->visible)	return;
	
	if(SDL_GetTicks()>abubble->enddisplay)
		abubble->visible=false;
		
	lineRGBA(ascreen, abubble->x, abubble->y-20, abubble->bubblex+20, abubble->bubbley-40, 0x99, 0x99, 0x99, 0xFF);
	lineRGBA(ascreen, abubble->x+1, abubble->y-20, abubble->bubblex+21, abubble->bubbley-40, 0x99, 0x99, 0x99, 0xFF);
	filledEllipseRGBA(ascreen, abubble->bubblex+20, abubble->bubbley-70, abubble->textwidth/2+15, 30, 0x99, 0x99, 0x99, 0xFF);
	bmpFontDrawStrL (ascreen, abubble->ff, 1, abubble->bubblex-abubble->textwidth/2+15	, abubble->bubbley-90, abubble->text);
}

void bubbleExit(Bubble *abubble)
{
	free(abubble->text);
	free(abubble);
}

