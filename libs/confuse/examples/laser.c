#include <string.h>
#include "confuse.h"

int main(void)
{
	cfg_bool_t verbose = cfg_false;
	int type=0;
	char *name = NULL;
	char *shotimage = NULL;
	char *iconimage = NULL;
	double speed = 0.0;
	int damage = 0;
	double range = 0.0;

	cfg_opt_t opts[] = {
		CFG_SIMPLE_INT("type", &type),
		CFG_SIMPLE_STR("name", &name),
		CFG_SIMPLE_STR("shotimage", &shotimage),
		CFG_SIMPLE_STR("iconimage", &iconimage),
		CFG_SIMPLE_FLOAT("speed", &speed),
		CFG_SIMPLE_INT("damage", &damage),
		CFG_SIMPLE_FLOAT("range", &range),
		CFG_END()
	};
	cfg_t *cfg;

	cfg = cfg_init(opts, 0);
	cfg_parse(cfg, "laser.conf");

	printf("type: %d\n", type);
	printf("name: %s\n", name);
	printf("shotimage: %s\n", shotimage);
	printf("iconimage: %s\n", iconimage);
	printf("speed: %f\n", speed);
	printf("damage: %d\n", damage);
	printf("range: %f\n", range);

//	printf("setting username to 'foo'\n");
	/* using cfg_setstr here is not necessary at all, the equivalent
	 * code is:
	 *   free(username);
	 *   username = strdup("foo");
	 */
/*	cfg_setstr(cfg, "user", "foo");
	printf("username: %s\n", username);

	{
		FILE *fp = fopen("simple.conf.out", "w");
		cfg_print(cfg, fp);
		fclose(fp);
	}
*/
	cfg_free(cfg);
	return 0;
}
