#ifndef MAN_H
#define MAN_H

#define DISPLAYTIME		2500
#define CUSTOMERTIMEOUT		3500

#define M_IDX_COUNT		8

#define M_IDX_WAVEONE	 	0
#define M_IDX_WAVETWO	 	1
#define M_IDX_WAVETHREE	 	2
#define M_IDX_WAVEFOUR		3
#define M_IDX_WALKLEFTONE	4
#define M_IDX_WALKLEFTTWO	5
#define M_IDX_WALKRIGHTONE	6
#define M_IDX_WALKRIGHTTWO	7

#define MAN_STAT_NORMAL		0x00
#define MAN_STAT_WALKING	0x01
#define MAN_STAT_VISIBLE	0x02

struct S_TAXIFACTORY;

typedef struct S_MAN{
	SDL_Surface *images[M_IDX_COUNT];
	double x,y;
	double dx;
	int walkendx;
	int drawframe;
	int plattform;
	unsigned int nextmanticks;
	unsigned int tickclock;
	unsigned int speedticks;
	Bubble *bubble;
	unsigned int status;
}Man;

Man *manInit (char *fnamestart, FontFactory *ff);
void manStart(Man *aman, SDL_Surface *colmap, int plattform);
void manDraw(SDL_Surface *ascreen, Man *aman, struct S_TAXIFACTORY *ataxi, LevelFactory *alevels, HudFactory *ahud);
void manDrawLanded(SDL_Surface *ascreen, Man *aman, struct S_TAXIFACTORY *ataxi, LevelFactory *alevels, HudFactory *ahud);
void manExit(Man *aman);

// internal stuff

#endif
