#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>  
#include <stdio.h>
#include <voxel.h>
#include "includes.h"
LPVOXELFACT                 GlpVoxFact;
SDL_Surface *       ddHeightMap=0;

/*-----------------------------------------------------------------------------*/
int voxelInit()
{
  GlpVoxFact=(LPVOXELFACT)calloc(sizeof(VOXELFACT),1);
  
  GlpVoxFact->viewX=MAPWIDTH*100000+(rand()%MAPWIDTH);
  GlpVoxFact->viewY=MAPHEIGHT*100000+(rand()%MAPHEIGHT);
  GlpVoxFact->viewAngle=360000+(rand()%360);
  
  GlpVoxFact->waterHeightAngle=0;
  
  GlpVoxFact->viewAngleDir=0.2;
  GlpVoxFact->viewMoveDir=1.5;

  if((ddHeightMap =imageloadalpha("images/heightmap.png"))==NULL) {
  	DEBUG("heightmap not found!!!");
  	return false;
  }
    
  return true;
}
/*-----------------------------------------------------------------------------*/
void voxelExit()
{
  if(ddHeightMap){
    SDL_FreeSurface(ddHeightMap);
    ddHeightMap=0;
  }
  free(GlpVoxFact);
}
/*-----------------------------------------------------------------------------*/
void voxelDraw(SDL_Surface *ascreen)
{
  double vx=GlpVoxFact->viewX, vy=GlpVoxFact->viewY, va=GlpVoxFact->viewAngle;
  double vxstart, vystart, vxdelta, vydelta, vylinedelta, vxlinedelta, zFact=1.0;
  int steps=MAXX/VOXSIZE, line;
  register int i;
  
  unsigned int color, height, yBuff[MAXX/VOXSIZE];
  LPVOXELFACT vf=GlpVoxFact;

  if((vf->waterHeightAngle+=1.3)>360) vf->waterHeightAngle-=360;
  vf->waterHeight=30.0*sin(vf->waterHeightAngle/180.0*M_PI)+48;

  for(i=0;i<steps;i++) yBuff[i]=MAXY;
  
  vxstart=vx+150*sin(va/180.0*M_PI);
  vystart=vy+150*cos(va/180.0*M_PI);
  vxdelta=sin((va-90)/180.0*M_PI);
  vydelta=cos((va-90)/180.0*M_PI);
  vxlinedelta=sin(va/180.0*M_PI);
  vylinedelta=cos(va/180.0*M_PI);

//  int daycolor=(120.0*sin(horizont.offset)+128.0);
  int daycolor=128;

  for(line=0;line<250;line++,vxstart+=vxlinedelta,vystart+=vylinedelta,zFact+=0.0025)
    for(i=0;i<steps;i++){
        vx=vxstart+vxdelta*(i-(steps>>1))*zFact;
        vy=vystart+vydelta*(i-(steps>>1))*zFact;
        
        color=grabpixel(ddHeightMap,((int)vx)%ddHeightMap->w,((int)vy)%ddHeightMap->h);
        height=color&0xFF;

        // color for water
        if(height<GlpVoxFact->waterHeight){ color=(0xFF+daycolor)+(((height)+(daycolor>>2))<<8); height=GlpVoxFact->waterHeight; }
        // color for mountains
        else {
          color=( ((color&0xFF)<<8)+((daycolor)<<16)+(0x7f-(daycolor>>1)));
        }
        yBuff[i]=DDDrawVoxel(ascreen, yBuff[i], height, color, line, i);
    }
  voxelSetPositions();
}

void voxelSetPositions()
{
  LPVOXELFACT vf=GlpVoxFact;

  vf->viewY+=(vf->viewMoveDir)*cos(vf->viewAngle/180.0*M_PI);
  vf->viewX+=(vf->viewMoveDir)*sin(vf->viewAngle/180.0*M_PI);

  vf->viewAngle+=vf->viewAngleDir;
}

/*-----------------------------------------------------------------------------*/
int DDDrawVoxel(SDL_Surface *ascreen, int maxheight, int height, unsigned int color, int line, int step)
{
  int top, bottom, left, right;
  
  top=MAXY-(height*1.0)-(line/1.5)+60;
  if(top>=maxheight) return maxheight;

  bottom=maxheight;
  left=step*VOXSIZE;
  right=step*VOXSIZE+VOXSIZE;

  boxRGBA(ascreen, left, top, right, bottom, (color&0xFF0000)>>16, (color&0xFF00)>>8, (color&0xFF), 0xFF);
  
  return(top);
}
