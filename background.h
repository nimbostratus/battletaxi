#ifndef BACKGROUND_H
#define BACKGROUND_H

// callback functions for background modules

typedef void* (backgroundinit_t) (void);
typedef void  (backgrounddraw_t) (SDL_Surface*, void*);
typedef void  (backgroundexit_t) (void*);

typedef struct S_BACKGROUND{
	backgroundinit_t *init;
	backgrounddraw_t *draw;
	backgroundexit_t *exit;
	void *factory;
	int	active;
	char *name;
}Background;

typedef struct S_BACKGROUNDFACTORY{
	Background **backgrounds;
	int count;
}BackgroundFactory;


BackgroundFactory *backgroundInit ();
void backgroundDraw (SDL_Surface *asurface, BackgroundFactory *abackground);
void backgroundExit(BackgroundFactory *abackground);

/*
	enables or disables a specific background. If the background was found by its
	name, the function returns true.
*/
int backgroundSetActive (BackgroundFactory *abackground, char *aname, int astate);

// internal functions

/*
	To be called from each bg effect module. Registers the module by assigning the name
	and functions to the factory. 
	The effect will be initialized by this function
*/
void bgregister (BackgroundFactory *afactory, char *aname, backgroundinit_t ainit, backgrounddraw_t adraw, backgroundexit_t aexit);


/*
	Gets the registered Background by its name and returns a pointer to it. If theres no
	Background with that name, the result will be NULL.
*/
Background *getBackgroundByName (BackgroundFactory *afactory, char *aname);


/*
	Disables all Backgrounds
*/
void backgroundDisableAll (BackgroundFactory *afactory);

#endif
