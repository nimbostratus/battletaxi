#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <time.h>
#include "includes.h"

void initRandom (void)
{
	FILE *f;
	int rand;

	/* initialize the random seed. If /dev/urandom is not available,
	   we'll use the current Timer value */
	if ((f=fopen("/dev/urandom", "r")))
	{
		if (fread(&rand, 1, sizeof(int), f) != 0)
			rand = (int)time(NULL);
		fclose(f);
	}
	else
		rand = (int)time(NULL);
	srand((unsigned)rand);
}

int main(int argc, char **argv)
{
	Game *g=gameInit();
	char fpstmp[30];
	initRandom();

	atexit (SDL_Quit);
	
	
	sndInit();
	sndStartBackgroundMusic ("sounds/sid.mp3");
	
	g->starttime = SDL_GetTicks();
	while(!g->quit){
		g->ticks = SDL_GetTicks()-g->starttime;
		if(g->ticks>500){
			g->fps=(double)g->framecounter/g->ticks*1000.0;
			g->framecounter=0;
			g->starttime = SDL_GetTicks();
		}
		
		SDL_PumpEvents();
		if (!g->pause){
			gameUpdate(g);
			blanksurface(g->screen);
			backgroundDraw(g->screen, g->bg);
			levelDraw(g->screen, g->levels);
			hudDraw (g->screen, g->fontfac, 1, g->hud);
			bubbleDraw(g->man->bubble,g->screen);
			manDraw(g->screen, g->man, g->taxi, g->levels, g->hud);
			weaponDraw(g->screen, g->weapons);
			messageDraw (g->screen, g->messages);
			inputDraw(g->screen, g->input, g->fontfac, 1, g->keystroke);
			
			if (!(g->input->status&INPUT_STAT_VISIBLE)){
				if(g->menuactive!=true && g->highscoreactive!=true && g->showhighscoretillticks>SDL_GetTicks()){
					sprintf(fpstmp, "New Highscore: %d Rank %d", g->hud->score, highscoreGetRank(g->highscore, g->hud->score));
					bmpFontDrawStrC(g->screen, g->fontfac, 0, MAXX/2, MAXY/2, fpstmp);
				}
				if (g->menuactive==true&&g->highscoreactive==false)
					mnuDraw (g->screen, g->mainmenu);
				if (g->highscoreactive)
					highscoreDraw(g->screen, g->highscore, g->fontfac);
				if (!g->menuactive && !g->highscoreactive)
					taxiDraw(g->screen, g->taxi, g->keystroke, g->levels->aktlevel->colmap);
			}
				
			sprintf(fpstmp, "%3.0f fps", g->fps);
			bmpFontDrawStrR(g->screen, g->fontfac, 1, MAXX, 0, fpstmp);
	
			SDL_Flip(g->screen); 
			g->framecounter++;
		}
	}
		
	gameExit(g);
	sndExit();
		
	return 0;
}
