#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <time.h>
#include "includes.h"

HighscoreFactory *highscoreInit ()
{
	HighscoreFactory *ret=(HighscoreFactory*)malloc(sizeof(HighscoreFactory));
	FILE *f=fopen(HIGHSCORENAME, "r");
	int i,j;

	for(i=0;i<MAXSCORES;i++){
		ret->scores[i].score=0;
		for(j=0;j<30;j++)
			ret->scores[i].name[j]=0;
		ret->scores[i].date=0;
		ret->scores[i].level=0;
	}
	if(f==NULL) return ret;
	
	fread(ret->scores, sizeof(Score), MAXSCORES, f);
	
	return ret;
}

void highscoreInsNewScore(HighscoreFactory *ahighscore, int score, char *name, int level)
{
	int i, j, n;
	
	for(i=0;i<MAXSCORES && ahighscore->scores[i].score>score;i++);
	if(i==MAXSCORES) return;
	i--;
	
	for(j=MAXSCORES-1;j>i;j--){
		ahighscore->scores[j].score=ahighscore->scores[j-1].score;
		strcpy(ahighscore->scores[j].name, ahighscore->scores[j-1].name);
		ahighscore->scores[j].date=ahighscore->scores[j-1].date;
		ahighscore->scores[j].level=ahighscore->scores[j-1].level;
	}
	ahighscore->scores[i+1].score=score;
	
	n=strlen(name);
	if(n>30)n=30;
	strncpy(ahighscore->scores[i+1].name, name, n);
	ahighscore->scores[i+1].name[n]=0;
	
	ahighscore->scores[i+1].date=time(NULL);
	ahighscore->scores[i+1].level=level;
}

int highscoreIsNew(HighscoreFactory *ahighscore, int score)
{
	if(ahighscore->scores[MAXSCORES-1].score<score)
		return true;

	return false;
}

int highscoreGetRank(HighscoreFactory *ahighscore, int score)
{
	int i;
	
	for(i=0;i<MAXSCORES && score<ahighscore->scores[i].score;i++);
	if(i<MAXSCORES)
		return(i+1);
	
	return(0);
}

void highscoreDraw (SDL_Surface *asurface, HighscoreFactory *ahighscore, FontFactory *fontfac)
{
	int i;
	char tmp[50];
	struct tm *blah;
	
	bmpFontDrawStrL(asurface, fontfac, 0, 100, 100, "Name");
	bmpFontDrawStrR(asurface, fontfac, 0, 500, 100, "Score");
	bmpFontDrawStrR(asurface, fontfac, 0, 650, 100, "Level");
	bmpFontDrawStrL(asurface, fontfac, 0, 700, 100, "Date");

	for(i=0;i<MAXSCORES-1;i++){
		if(ahighscore->scores[i].name[0]!=0)
			bmpFontDrawStrL(asurface, fontfac, 0, 100, i*40+140, ahighscore->scores[i].name);
		else
			bmpFontDrawStrL(asurface, fontfac, 0, 100, i*40+140, "Nobody (yet)");
		sprintf(tmp, "%d", ahighscore->scores[i].score);
		bmpFontDrawStrR(asurface, fontfac, 0, 500, i*40+140, tmp);
		
		blah=localtime((const time_t*)&(ahighscore->scores[i].date));
		
		if(ahighscore->scores[i].date>0)
			sprintf(tmp, "%02d.%02d.%04d %02d:%02d", blah->tm_mday, blah->tm_mon+1, blah->tm_year+1900, blah->tm_hour, blah->tm_min);
		else
			tmp[0]=0;
		bmpFontDrawStrL(asurface, fontfac, 0, 700, i*40+140, tmp);
		
		sprintf(tmp, "%d", ahighscore->scores[i].level);
		if(ahighscore->scores[i].level!=0) 
			bmpFontDrawStrR(asurface, fontfac, 0, 650, i*40+140, tmp);
	}
}

void highscoreExit(HighscoreFactory *ahighscore)
{
	FILE *f=fopen(HIGHSCORENAME, "w");
	assert(f);
	
	fwrite(ahighscore->scores, sizeof(Score), MAXSCORES, f);

	free(ahighscore);
}
