#ifndef MENU_H
#define MENU_H

typedef enum {
	MENUITEM_RADIO=1,	// MenuItem which may be switched on or off
	MENUITEM_NUMERIC=2,	// MenuItem with a numeric Value
	MENUITEM_LIST=3,	// MenuItem containing a list to choose from
	MENUITEM_MENU=4		// MenuItem starting another menu..
} MenuItemKind;

typedef enum {
	MENUACTION_INCREASE=0,
	MENUACTION_DECREASE=1,
	MENUACTION_ACTIVATE=2,
	MENUACTION_NEXT=3,
	MENUACTION_PREV=4
} MenuAction;

struct S_MENUITEM;

typedef void (mnucallb_t) (struct S_MENUITEM *, void *);

typedef struct S_MENUITEM
{
	MenuItemKind kind;
	char *name;
	char *text;
	char **list;
	int listselected;
	int listcount;
	int minvalue;
	int maxvalue;
	int value;
	int selected;			// for radio items
	mnucallb_t *event;		// callback function
	void *userdata;			// data omitted by user for callback function
	struct S_MENUITEM *next;
	struct S_MENUITEM *sub;
} Menu;

typedef struct S_MENUFACTORY
{
	Menu *top;
	Menu *active;
	SDL_Surface *selectorr;
	SDL_Surface *selectorl;
	FontFactory *fontfac;
	SDL_Surface *destsurface;
	int maxtextwidth;
	unsigned int count;
	int fontnum;
	
	unsigned int lastletterpress;
	
} MenuFactory;

MenuFactory *mnuInit (char *aselectorr,
		char *aselectorl, 
		FontFactory *afontfac,
		SDL_Surface *adestsurface);
		
void mnuExit (MenuFactory *afactory);

Menu *mnuAddItem (MenuFactory *amenu, 
		Menu *aparent,
		const char *aname, 
		const MenuItemKind akind, 
		const char *atext, 
		char **alist,
		int alistselected, 
		int aminvalue, 
	        int amaxvalue, 
		int avalue,
		int aselected,
		mnucallb_t aevent);

		
inline Menu *mnuAddMenuItem (MenuFactory *amenu, const char *aname, const char *atext);
inline Menu *mnuAddRadioItem (MenuFactory *amenu, const char *aname, const char *atext, int aselected);
inline Menu *mnuAddNumericItem (MenuFactory *amenu, const char *aname, const char *atext, int amin, int amax, int avalue );
inline Menu *mnuAddListItem (MenuFactory *amenu, const char *aname, const char *atext, char **alist, int alistselected);
inline void mnuAttachEvent (MenuFactory *amenu, char *aname, mnucallb_t aevent, void *adata);

inline MenuFactory *mnuAddSubMenuItem (MenuFactory *aparent, const char *aname, const char *atext);
inline MenuFactory *mnuAddSubRadioItem (MenuFactory *aparent, const char *aname, const char *atext, int selected);
inline MenuFactory *mnuAddSubNumericItem (MenuFactory *aparent, const char *aname, const char *atext, int amin, int amax, int avalue );
inline MenuFactory *mnuAddSubListItem (MenuFactory *aparent, const char *aname, const char *atext, const char **alist, int listselected);

void mnuDraw (SDL_Surface *ascreen, MenuFactory *amenu);
int mnuProcess (MenuFactory *amenu, MenuAction aaction); // returns 1 if a change was permitted, or 0

Menu *mnuGetItemByName (const Menu *amenu, const char *aname);
Menu *mnuGetActive (const Menu *amenu);
void mnuSetActive (MenuFactory *amenu, const char *aname);



#endif
