#include <stdlib.h>
#include <assert.h>
#include "includes.h"

int initialheight, initialwidth;

SDL_Surface* initsdl (const int aw, const int ah, const int ad)
{
	SDL_Surface *tscreen;

	if((SDL_Init(SDL_INIT_VIDEO)==-1))
	{ 
		fprintf(stderr, "Could not initialize SDL: %s.\n", SDL_GetError());
		exit(-1);
	}
	
	tscreen = SDL_SetVideoMode (aw, ah, ad, SDL_HWSURFACE|SDL_DOUBLEBUF);
// 	tscreen = SDL_SetVideoMode (aw, ah, ad, SDL_HWSURFACE|SDL_ASYNCBLIT|SDL_DOUBLEBUF);
	if (tscreen == NULL)
	{
		fprintf (stderr, "Could not set Videomode (%dx%dx%d) : %s\n",aw, ah, ad, SDL_GetError());
		exit (1);
	}
	
	return tscreen;
}

void shutdownsdl (SDL_Surface* ascreen)
{
	if (ascreen)
	{
		SDL_FreeSurface (ascreen);
		ascreen = NULL;
	}
	SDL_Quit();
}

int drawpixel (SDL_Surface* ascreen, const int ax, const int ay, const int ar, const int ag, const int ab)
{
	int bpp = ascreen->format->BytesPerPixel;
	/* Here p is the address to the pixel we want to set */
	Uint8 *p = (Uint8 *)ascreen->pixels + ay * ascreen->pitch + ax * bpp;
	Uint32 color = SDL_MapRGB(ascreen->format, ar, ag, ab);


	if ((ax > ascreen->w-1) || (ay > ascreen->h-1) || (ax < 0) || (ay < 0))
	{
//		fprintf (stderr, "Warning: Pixel drawed outside screen at %d:%d\n", ax, ay);
		return 0;
	}

	switch(bpp) {
	case 1:
		*p = color;
		break;

	case 2:
		*(Uint16 *)p = color;
		break;

	case 3:
		if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
		p[0] = (color  >> 16) & 0xff;
		p[1] = (color  >> 8) & 0xff;
		p[2] = color  & 0xff;
		} else {
		p[0] = color  & 0xff;
		p[1] = (color  >> 8) & 0xff;
		p[2] = (color  >> 16) & 0xff;
		}
		break;

	case 4:
		*(Uint32 *)p = color;
		break;
	}
//	SDL_UpdateRect(ascreen, ax, ay, 1, 1);
	return 1;
}

void switchfullscreen (void)
{
	SDL_Surface *tmp = SDL_GetVideoSurface();
	Uint32 flags;
	int x,y,bpp;
	if (tmp)
	{
		flags = tmp->flags ^ SDL_FULLSCREEN;
		x = tmp->w;
		y = tmp->h;
		bpp = tmp->format->BitsPerPixel;
		SDL_SetVideoMode (x, y, bpp, flags);
	}
}

void blanksurface(SDL_Surface *ascreen)
{
	SDL_Rect tmp;
	assert (ascreen);
	tmp.x = tmp.y = 0;
	tmp.h = ascreen->h;
	tmp.w = ascreen->w;
	SDL_FillRect (ascreen, &tmp, 0);
}


void drawline(SDL_Surface *ascreen, int x, int y, int x2, int y2, int ar, int ag, int ab)
{
	int yL = 0;
	int sL = y2-y;
	int lL = x2-x;
	int j = 0;
	if(abs(sL)>abs(lL))
	{
		int swap = sL;
		sL = lL;
		lL = swap;
		yL = 1;
	}
	int dI;
	if(lL == 0) dI = 0;
	else dI = (sL << 16)/lL;
	if(yL)
	{
		if(lL>0)
		{
			lL += y;
			for(j=0x8000+(x << 16); y<=lL; ++y){
			drawpixel(ascreen, j >> 16, y, ar, ag, ab); //define your own putPix mofo
			j += dI;
		}
		return;
	}
		lL += y;
		for(j=0x8000+(x << 16); y>=lL; --y)
		{
			drawpixel(ascreen, j >> 16, y, ar, ag, ab);
			j -= dI;
		}
		return;
	}
	if(lL>0)
	{
		lL += x;
		for(j=0x8000+(y << 16); x<=lL; ++x)
		{
			drawpixel(ascreen, x, j >> 16, ar, ag, ab);
			j += dI;
		}
		return;
	}
	lL += x;
	for(j=0x8000+(y << 16); x>=lL; --x)
	{
		drawpixel(ascreen, x, j >> 16, ar ,ag, ab);
		j -= dI;
	}
}	

int begindrawing (SDL_Surface* ascreen)
{
	if ( SDL_MUSTLOCK(ascreen) )
	{
		if ( SDL_LockSurface(ascreen) < 0 )
		{
			fprintf(stderr, "Can't lock screen: %s\n", SDL_GetError());
			return 0;
		} else {
			return 1;
		}
	}
	return 1;
}

void enddrawing (SDL_Surface* ascreen)
{
	if ( SDL_MUSTLOCK(ascreen) )
	{
		SDL_UnlockSurface(ascreen);
	}
}


Uint32 grabpixel (SDL_Surface *ascreen, int ax, int ay)
{
	assert (ascreen != NULL);

	int bpp = ascreen->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)ascreen->pixels + ay * ascreen->pitch + ax * bpp;

	if ((ax > ascreen->w) || (ay > ascreen->h) || (ax < 0) || (ay < 0))
	{
//		fprintf (stderr, "Warning: Trying to read Pixel outside screen at %d:%d\n", ax, ay);
		return 0;
	}

	switch(bpp) {
	case 1:
		return *p;

	case 2:
		return *(Uint16 *)p;

	case 3:
		if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
			return p[0] << 16 | p[1] << 8 | p[2];
		else
			return p[0] | p[1] << 8 | p[2] << 16;

	case 4:
		return *(Uint32 *)p;

	default:
		return 0;
    }
}


int drawGetFirstColor(Point *ret, SDL_Surface *acolmap, int matchcolor)
{
	int x,y;
	
	for(y=0;y<acolmap->h;y++)
		for(x=0;x<acolmap->w;x++)
			if((grabpixel(acolmap, x, y)&0xFFFFFF)==matchcolor){
				ret->x=x;
				ret->y=y;
				return true;
			}
	
	ret->x=0;
	ret->y=0;
	return false;
}

SDL_Surface *imageloadalpha (char *afile)
{
	assert (afile);
	SDL_Surface *tmp, *ret;
	
	tmp = IMG_Load (afile);
	
	if (tmp == NULL)
	{
/*		fprintf (stderr, "Critical: could not load image %s", afile);
		fflush (stdout);
		exit (1);*/
		return NULL;
	}
	ret = SDL_DisplayFormatAlpha (tmp);
	
	if (ret == NULL)
	{
		fprintf (stderr, "Critical: could not convert image %s", afile);
		fflush (stdout);
		exit (2);
	}
	
	free (tmp);
	return ret;
}
