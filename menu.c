
#include <assert.h>
#include <malloc.h>
#include <string.h>
#include "includes.h"

// internal: count list items

// inline int mnuCountListItems (char **alist)
// {
// 	assert (alist);
// 	return sizeof (alist)
// }

//internal : count menu items at one level

int mnuCountItems (Menu *amenu)
{
	assert (amenu);
	Menu *tmp;
	int i=1;

	tmp = amenu;
	while (tmp->next != NULL)
	{
		tmp=tmp->next;
		i++;
	}
	return i;
}

// internal : calculate the space between center and seperator

int mnuCountSeperatorMargin (Menu *amenu, FontFactory *afont, int afontnum)
{
	assert (amenu);

	Menu *tmp;
	int width=0;
	int stringwidth;

	tmp = amenu;
	while (tmp->next != NULL)
	{
		tmp=tmp->next;
		stringwidth = bmpFontTextWidth (afont, afontnum, tmp->text);
		width = width < stringwidth ? stringwidth : width;
	}
	return (width/2);
}

//internal: copy one string list to another; returns created array
char **mnuCopyList (char **src, int acount)
{
	char **tmplist;
	char **ret;
	tmplist = src;
	int i;
	
	assert (src);
	
	ret = (char **) malloc ( sizeof ( char** ) * (acount + 1) );
	
	for (i=0; i < acount; i++)
	{
		ret[i] = (char *) malloc ( strlen ( src[i] ) + 1 );
		strcpy (ret[i], src[i]);
	}
	ret[i] = NULL;
	
	return ret;
}

MenuFactory *mnuInit (char *aselectorr, 
		char *aselectorl, 
		FontFactory *afontfac,
		SDL_Surface *adestsurface)
{
	MenuFactory *ret;
	
	ret = (MenuFactory*)malloc(sizeof(MenuFactory));
	ret->top = NULL;
	ret->active = NULL;
	ret->selectorr = imageloadalpha (aselectorr);
	ret->selectorl = imageloadalpha (aselectorl);
	ret->fontfac = afontfac;
	ret->fontnum = 0;
	ret->lastletterpress = 0;
	ret->count = 0;
	ret->maxtextwidth = 0;
	ret->destsurface = adestsurface;
	return ret;
}
		
void mnuExit (MenuFactory *afactory)
{
	Menu *walker, *last;
	int i=0;

	assert (afactory);

	walker = afactory->top;
	while (walker != NULL)
	{
		free (walker->name);
		walker->name = NULL;

		free (walker->text);
		walker->text = NULL;

		if (walker->list != NULL)
		{
			for (i=0; i<walker->listcount; i++)
			{
				free (walker->list[i]);
				walker->list[i] = NULL;
			}

		free (walker->list);
		walker->list = NULL;
		}


		walker->event = NULL;
		walker->userdata = NULL;

		last = walker;
		walker = walker->next;
		free (last);
		last = NULL;
	}

	if (afactory->selectorr != NULL)
		SDL_FreeSurface (afactory->selectorr);
	if (afactory->selectorl != NULL)
		SDL_FreeSurface (afactory->selectorl);
	free (afactory);
	afactory = NULL;
}


Menu *mnuAddItem (MenuFactory *amenu, 
		Menu *aparent,
		const char *aname, 
		const MenuItemKind akind, 
		const char *atext, 
		char **alist,
		int alistselected, 
		int aminvalue, 
	        int amaxvalue, 
		int avalue,
		int aselected,
		mnucallb_t aevent)
{
	assert (aname);
	assert (atext);

	Menu *tmp = (Menu*) malloc (sizeof (Menu));
	Menu *walker; // temp. pointer
	int tmptextwidth;

	assert (tmp);

	tmp->name = (char*) malloc (strlen (aname) + 1);
	tmp->text = (char*) malloc (strlen (atext) + 1);
	assert (tmp->name);
	assert (tmp->text);

	strcpy (tmp->name, aname);
	strcpy (tmp->text, atext);

	tmp->next = NULL;
	tmp->sub  = NULL;
	tmp->list = NULL;
	tmp->event = aevent;
	tmp->kind = akind;
	tmp->userdata = NULL;
	

	switch (akind)
	{
		case MENUITEM_RADIO :	tmp->selected = aselected;
					break;
		case MENUITEM_NUMERIC :	tmp->minvalue = aminvalue;
					tmp->maxvalue = amaxvalue;
					tmp->value = avalue;
					break;
		case MENUITEM_MENU :	break;
		case MENUITEM_LIST :	for(tmp->listcount=0; alist[tmp->listcount]!=NULL; tmp->listcount++);
					tmp->list = mnuCopyList ((char**)alist, tmp->listcount);
					tmp->listselected = alistselected;
	}
	if (aparent != NULL)
		aparent->sub = tmp;
	if (amenu->top != NULL)
	{
		walker = amenu->top;
		while (walker->next != NULL) walker = walker->next;
		walker->next = tmp;
	} else {
		amenu->top = tmp;
	}

	amenu->count++;
	tmptextwidth = bmpFontTextWidth (amenu->fontfac, amenu->fontnum, tmp->text);
	amenu->maxtextwidth = tmptextwidth > amenu->maxtextwidth ? tmptextwidth : amenu->maxtextwidth;
	return tmp;
}

inline Menu *mnuAddMenuItem (MenuFactory *amenu, const char *aname, const char *atext)
{
	return mnuAddItem (amenu, NULL, aname, MENUITEM_MENU, atext, NULL, 0, 0, 0, 0, 0, NULL);
}

inline Menu *mnuAddRadioItem (MenuFactory *amenu, const char *aname, const char *atext, int aselected)
{
	return mnuAddItem (amenu, NULL, aname, MENUITEM_RADIO, atext, NULL, 0, 0, 0, 0, aselected, NULL);
}

inline Menu *mnuAddNumericItem (MenuFactory *amenu, const char *aname, const char *atext, int amin, int amax, int avalue )
{
	return mnuAddItem (amenu, NULL, aname, MENUITEM_NUMERIC, atext, NULL, 0, amin, amax, avalue, 0, NULL);
}

inline Menu *mnuAddListItem (MenuFactory *amenu, const char *aname, const char *atext, char **alist, int alistselected)
{
	return mnuAddItem (amenu, NULL, aname, MENUITEM_LIST, atext, alist, alistselected, 0, 0, 0, 0, NULL);
}
inline void mnuAttachEvent (MenuFactory *amenu, char *aname, mnucallb_t aevent, void* adata)
{
	assert (amenu);
	assert (aname);
	
	Menu *tmp = mnuGetItemByName (amenu->top, aname);
	tmp->userdata = adata;
	tmp->event = aevent;
}

inline MenuFactory *mnuAddSubMenuItem (MenuFactory *aparent, const char *aname, const char *atext)
{
	return aparent;
}

inline MenuFactory *mnuAddSubRadioItem (MenuFactory *aparent, const char *aname, const char *atext, int selected)
{
	return aparent;
}

inline MenuFactory *mnuAddSubNumericItem (MenuFactory *aparent, const char *aname, const char *atext, int amin, int amax, int avalue )
{
	return aparent;
}

inline MenuFactory *mnuAddSubListItem (MenuFactory *aparent, const char *aname, const char *atext, const char **alist, int listselected)
{
	return aparent;
}

void mnuDraw (SDL_Surface *ascreen, MenuFactory *amenu)
{
	assert (amenu);
	assert (ascreen);

	int count = amenu->count;
	
	// some drawing stuff
	int height = amenu->selectorl->h;
	int sep = amenu->destsurface->h / (count * amenu->fontfac->fonts[amenu->fontnum]->Surface->h);
	int menuheight = (count * height) + ( (count-1) * sep);
	int topmargin = (amenu->destsurface->h/2) - (menuheight/2);
	int seperatormargin = amenu->maxtextwidth;
	SDL_Rect rcsrc, rcdestr, rcdestl;
	int x = amenu->destsurface->w/2;
	int y;
	
	int num = 1;
	char *tmpstring = (char *)malloc (strlen (amenu->top->text)+100);
	
	Menu *walker;
	rcsrc.y=rcsrc.x=0;
	rcsrc.h=amenu->selectorl->h;
	rcsrc.w=amenu->selectorl->w;
	rcdestl.w=amenu->selectorr->w;
	rcdestl.h=amenu->selectorr->h;
	rcdestl.x=x - amenu->selectorl->w - 20 - seperatormargin;

	rcdestr.w = amenu->selectorr->w;
	rcdestr.h = amenu->selectorr->h;
	rcdestr.x = x + amenu->selectorr->w - 70 + seperatormargin;

	walker = amenu->top;
	
	while (walker != NULL)
	{
		y = topmargin + (num * height) + ( (num-1) * sep);
		switch (walker->kind){
		case MENUITEM_RADIO :	break;
		case MENUITEM_NUMERIC :	sprintf (tmpstring, walker->text, walker->value);
					bmpFontDrawStrC (amenu->destsurface, amenu->fontfac, amenu->fontnum, x, y, tmpstring);
					break;
		case MENUITEM_MENU :	bmpFontDrawStrC (amenu->destsurface, amenu->fontfac, amenu->fontnum, x, y, walker->text);
					break;
		case MENUITEM_LIST :	sprintf (tmpstring, walker->text, walker->list[walker->listselected]);
					bmpFontDrawStrC (amenu->destsurface, amenu->fontfac, amenu->fontnum, x, y, tmpstring);
					break;		
		}
		
		rcdestr.y = y;
		rcdestl.y = y;
		if (walker == amenu->active)
		{
			SDL_BlitSurface (amenu->selectorl, &rcsrc, amenu->destsurface, &rcdestl);
			SDL_BlitSurface (amenu->selectorr, &rcsrc, amenu->destsurface, &rcdestr);
		}

		walker=walker->next;
		num++;
	}
	free (tmpstring);
}

int mnuProcess (MenuFactory *amenu, MenuAction aaction)
{
	Menu *walker  = amenu->top;
	if(SDL_GetTicks() - amenu->lastletterpress > 150){

	switch (aaction) {
		case MENUACTION_INCREASE :
			if (amenu->active->kind == MENUITEM_NUMERIC)
			{
				if (amenu->active->value < amenu->active->maxvalue)
				{
					amenu->active->value++;
					if (amenu->active->event != NULL)
						amenu->active->event (amenu->active, amenu->active->userdata);
				}
			}
			else if (amenu->active->kind == MENUITEM_LIST)
			{
				if (amenu->active->listselected < amenu->active->listcount - 1)
				{
					amenu->active->listselected++;
					if (amenu->active->event != NULL)
						amenu->active->event (amenu->active, amenu->active->userdata);
				}
			}
			break;
		case MENUACTION_DECREASE :
			if (amenu->active->kind == MENUITEM_NUMERIC)
			{
				if (amenu->active->value > amenu->active->minvalue)
				{
					amenu->active->value--;
					if (amenu->active->event != NULL)
						amenu->active->event (amenu->active, amenu->active->userdata);
				}
			}
			else if (amenu->active->kind == MENUITEM_LIST)
			{
				if (amenu->active->listselected > 0)
				{
					amenu->active->listselected--;
					if (amenu->active->event != NULL)
						amenu->active->event (amenu->active, amenu->active->userdata);
				}
			}
			break;
		case MENUACTION_NEXT :
			if (amenu->active->next != NULL)
				amenu->active = amenu->active->next;
			break;

		case MENUACTION_PREV :
			if (walker != amenu->active)
			{
				while ((walker->next != NULL) && (walker->next != amenu->active) )
					walker = walker->next;
				amenu->active = walker;
			}
			break;
		case MENUACTION_ACTIVATE :
			if (amenu->active->kind == MENUITEM_MENU)
			{
					if (amenu->active->event != NULL)
						amenu->active->event (amenu->active, amenu->active->userdata);
			}
			break;
		}
	amenu->lastletterpress = SDL_GetTicks();
	}
	return 0;
}


Menu *mnuGetItemByName (const Menu *amenu, const char *aname)
{
	Menu *tmp=(Menu*)amenu;

	while (tmp->next != NULL)
	{
		if (! strcmp(tmp->name, aname))
			return tmp;
		tmp = tmp->next;
	}
	if (! strcmp(tmp->name, aname))
		return tmp;
	
	return NULL;
}

Menu *mnuGetActive (const Menu *amenu)
{
	return NULL;
}

void mnuSetActive (MenuFactory *amenu, const char *aname)
{
	assert (amenu);
	assert (aname);
	
	Menu *tmp=mnuGetItemByName (amenu->top, aname);
	amenu->active = tmp;
}
