#include <assert.h>
#include <malloc.h>
#include <string.h>
#include "drawings.h"
#include "SFont.h"
#include "includes.h"


FontFactory *bmpFactoryInit (char **afiles)
{
	assert (**afiles);
	
	FontFactory *ret = (FontFactory *) malloc (sizeof(FontFactory) );
	int i;

	for (ret->count=0; afiles[ret->count]!=NULL; ret->count++);

	ret->fonts=(SFont_Font**) malloc ( sizeof(SFont_Font*) * (ret->count+1) );
	ret->fonts[ret->count]=NULL;

	for (i=0; i<ret->count ; i++)
		ret->fonts[i] = bmpFontLoad (afiles[i]);

	return ret;
}

void *bmpFactoryExit (FontFactory *afactory)
{
	assert (afactory);
	int i;

	for (i=0; i<afactory->count; i++)
		bmpFontFree (afactory->fonts[i]);
	free (afactory->fonts);
	afactory->fonts = NULL;
	free (afactory);
	afactory = NULL;
	return NULL;
}

int bmpFontDrawStrL (SDL_Surface *adest, FontFactory *afont, int afontnum, int ax, int ay, char *atext)
{
	assert (adest);
	assert (afont);
	assert (atext);

	SFont_Font *tmpFont;

	tmpFont = afont->fonts[afontnum];
	SFont_Write (adest, tmpFont, ax, ay, atext);

	return 1;
}

inline int bmpFontDrawStrC (SDL_Surface *adest, FontFactory *afont, int afontnum, int ax, int ay, char *atext)
{
	return bmpFontDrawStrL (adest, afont, afontnum, ax - bmpFontTextWidth (afont, afontnum, atext)/2, ay, atext);
}

inline int bmpFontDrawStrR (SDL_Surface *adest, FontFactory *afont, int afontnum, int ax, int ay, char *atext)
{
	return bmpFontDrawStrL (adest, afont, afontnum, ax - bmpFontTextWidth (afont, afontnum, atext), ay, atext);
}

int bmpFontTextWidth (FontFactory *afactory, int afontnum, char *atext)
{
	SFont_Font *tmpFont;

	assert (afactory);
	assert (atext);

	tmpFont = afactory->fonts[afontnum];

	return SFont_TextWidth(tmpFont, atext);
}

int bmpFontTextHeight (FontFactory *afactory, int afontnum)
{
	assert (afactory);
	return afactory->fonts[afontnum]->Surface->h - 1;
}

// internal functions

SFont_Font *bmpFontLoad (char *fname)
{
	assert (fname);
	SFont_Font *ret;
	SDL_Surface *tmp;

	tmp = imageloadalpha (fname);
	ret = SFont_InitFont (tmp);

	return ret;
}

void bmpFontFree (SFont_Font *afont)
{
	assert (afont);
	assert (afont->Surface);
	SDL_FreeSurface (afont->Surface);
	afont->Surface = NULL;
	free (afont);
	afont = NULL;
}
