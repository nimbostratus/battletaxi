
#ifndef LEVEL_H
#define LEVEL_H

typedef struct S_LEVEL{
	SDL_Surface *img;
	SDL_Surface *colmap;
	char *name;
	double gravity;
	double liftoffspeed;
	int plattformcount;
	char **bgeffects;    // named background effects, null terminated
	int numbgeffects;    // count of background effects
	struct S_LEVEL  *next;
}Level;

typedef struct S_LEVELFACTORY{
	Level *levellist;
	Level *aktlevel;
	int aktlevelnum;
	int maxlevel;
}LevelFactory;

LevelFactory *levelInit (char *apath);
void levelSwitch(LevelFactory *alevel, BackgroundFactory *abackground, int number);
void levelDraw (SDL_Surface *ascreen, LevelFactory *alevel);
void levelExit(LevelFactory *alevel);
int levelGetRandPlattform(LevelFactory *alevels);

// internal stuff

#endif
