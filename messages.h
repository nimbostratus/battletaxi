#ifndef MESSAGES_H
#define MESSAGES_H

#include "fonts.h"

typedef struct S_MESSAGEFACTORY{
	char *text;
	unsigned int enddisplay;
	int visible;
	FontFactory *ff;
}MessageFactory;

MessageFactory *messageInit (int amessageticks, FontFactory *aff);
void messageExit (MessageFactory *afactory);
void messageDraw (SDL_Surface *ascreen, MessageFactory *afactory);

void messageThrow (char *amessage);

#endif
