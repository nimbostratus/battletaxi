#ifndef SPLASH_H
#define SPLASH_H

void splashInit (char *apicture, char *asteppicture, int asteps);
void splashStep (char *amessage);
void splashDone (void);

#endif
