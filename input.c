#include <assert.h>
#include <malloc.h>
#include <string.h>
#include "includes.h"

InputFactory *inputInit ()
{
	InputFactory *ret=(InputFactory*)malloc(sizeof(InputFactory));
	
	ret->status=INPUT_STAT_NORMAL;
	ret->aktbox=ret->boxes=(InputBox*)malloc(sizeof(InputBox));
	memset(ret->keycopy, 0, 322);
	
	ret->boxes->next=NULL;
	
	return ret;
}

void inputDraw (SDL_Surface *asurface, InputFactory *ainput, FontFactory *afont, int afontnum, Uint8 *akeystroke)
{
	#define IWIDTH	400
	#define BORDER	7
	
	int midx=MAXX/2, midy=MAXY/2, pwidth, pheight, i;
	if(!(ainput->status&INPUT_STAT_VISIBLE))
		return;
	
	pwidth=bmpFontTextWidth(afont, afontnum, ainput->aktbox->prompt);
	pheight=bmpFontTextHeight(afont, afontnum);
	boxRGBA(asurface, midx-pwidth/2-IWIDTH/2-BORDER, midy-pheight/2-BORDER, midx+pwidth/2+IWIDTH/2+BORDER, midy+pheight/2+BORDER, 
		0x99, 0x99, 0x99, 0x99);
	bmpFontDrawStrL(asurface, afont, afontnum, midx-pwidth/2-IWIDTH/2, midy-pheight/2, ainput->aktbox->prompt);
	bmpFontDrawStrL(asurface, afont, afontnum, (midx-pwidth/2-IWIDTH/2)+pwidth, midy-pheight/2, ainput->aktbox->input);
	
	if(akeystroke[SDLK_ESCAPE]){
		ainput->status&=~INPUT_STAT_VISIBLE;
		return;
	}
	
	if(ainput->acceptinputat>SDL_GetTicks()) return;
	
	for(i=32;i<128;i++){
		if(akeystroke[i]&&!ainput->keycopy[i]){
			ainput->aktbox->input=realloc(ainput->aktbox->input, strlen(ainput->aktbox->input)+2);
			ainput->aktbox->input[strlen(ainput->aktbox->input)+1]=0;
			if(akeystroke[SDLK_RSHIFT] || akeystroke[SDLK_LSHIFT])
				ainput->aktbox->input[strlen(ainput->aktbox->input)]=(Uint8)i+('A'-'a');
			else
				ainput->aktbox->input[strlen(ainput->aktbox->input)]=(Uint8)i;
		}
	}
	
	if(akeystroke[SDLK_BACKSPACE]&& !ainput->keycopy[SDLK_BACKSPACE])
		ainput->aktbox->input[strlen(ainput->aktbox->input)-1]=0;
	
	if(akeystroke[SDLK_RETURN]){
		ainput->status&=~INPUT_STAT_VISIBLE;
		ainput->aktbox->action(ainput->aktbox->userdata, ainput->aktbox->input);
	}
	
	memcpy(ainput->keycopy, akeystroke, 322);
}

void inputExit(InputFactory *ainput)
{
	InputBox *runner=ainput->boxes, *tmp;
	
	while(runner->next!=NULL){
		tmp=runner->next;
		free(runner->refname);
		free(runner->prompt);
		free(runner->input);
		free(runner);
		runner=tmp;
	}
	free(ainput);
}

void inputInsBox(InputFactory *ainput, char *arefname, char *prompt, inputcallb_t *aaction, void *auserdata)
{
	InputBox *runner=ainput->boxes;
	
	while(runner->next!=NULL) runner=runner->next;
	
	runner->next=(InputBox *)malloc(sizeof(InputBox));
	runner->next->next=NULL;
	
	runner->refname=(char*)malloc(strlen(arefname)+1);
	strcpy(runner->refname, arefname);
	
	runner->prompt=(char*)malloc(strlen(prompt)+1);
	strcpy(runner->prompt, prompt);
	
	runner->input=malloc(1);
	runner->input[0]=0;
	
	runner->userdata=auserdata;
	runner->action=aaction;
}

void inputShowBox(InputFactory *ainput, char *arefname)
{
	InputBox *runner=ainput->boxes;
	
	while(runner->next!=NULL && strcmp(runner->refname, arefname))
		runner=runner->next;
	
	if(runner->next==NULL) return;
	
	memset(ainput->keycopy, 0, 322);
	
	runner->input[0]=0;
	
	ainput->aktbox=runner;
	ainput->status|=INPUT_STAT_VISIBLE;
	ainput->acceptinputat=SDL_GetTicks()+100;
}

