#include "includes.h"

int mysteps;
int myactstep;
SDL_Surface *myimg;
SDL_Surface *mystep;


void splashInit (char *apicture, char *asteppicture, int asteps)
{
	assert (apicture);
	assert (asteppicture);

	myimg = imageloadalpha (apicture);
	mystep = imageloadalpha (asteppicture);
	mysteps = asteps;
	myactstep = 0;
}

void splashStep (char *amessage)
{
	assert (myimg);
	assert (mystep);
	SDL_Rect rcsrc, rcdst;
	SDL_Surface *videosurface = SDL_GetVideoSurface();
	assert (videosurface);
	
	if (myactstep < mysteps)
		myactstep++;

	rcsrc.x = rcsrc.y = rcdst.x = rcdst.y = 0;
	rcsrc.w = rcdst.w = myimg->w;
	rcsrc.h = rcdst.h = myimg->h;
	SDL_BlitSurface (myimg, &rcsrc, videosurface, &rcdst);

	if (amessage != NULL)
	{
		messageThrow (amessage);
		messageDraw (videosurface, NULL);
	}

	
	SDL_Flip(videosurface);
}

void splashDone (void)
{
	assert (myimg);
	assert (mystep);
	SDL_FreeSurface (myimg);
	SDL_FreeSurface (mystep);
	myimg = NULL;
	mystep = NULL;
}

