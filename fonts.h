#ifndef FONTS_H
#define FONTS_H

#include "SFont.h"

typedef struct {
	SFont_Font **fonts;
	int count;
} FontFactory;

FontFactory *bmpFactoryInit (char **afiles);
void *bmpFactoryExit (FontFactory *afactory);

int bmpFontDrawStrL (SDL_Surface *adest, FontFactory *afont, int afontnum, int ax, int ay, char *atext);
int bmpFontDrawStrC (SDL_Surface *adest, FontFactory *afont, int afontnum, int ax, int ay, char *atext);
int bmpFontDrawStrR (SDL_Surface *adest, FontFactory *afont, int afontnum, int ax, int ay, char *atext);

int bmpFontTextWidth (FontFactory *afactory, int afontnum, char *atext);
int bmpFontTextHeight (FontFactory *afactory, int afontnum);

// internal functions

SFont_Font *bmpFontLoad (char *fname);
void bmpFontFree (SFont_Font *afont);

#endif
