CC = gcc
#DEBUG = -DNDEBUG
GDBG=-ggdb
#PROFILE = -p
PROFILE =
INCLUDES = -I./libs/confuse -I./libs/sfont
CFLAGS =  `sdl-config --cflags`  -I. ${INCLUDES} -DHAVE_CONFIG_H ${DEBUG} ${GDBG} ${PROFILE} -Wall
LDFLAGS = `sdl-config --libs` -lSDL_image -lSDL_gfx -lSDL_mixer
SOURCES = $(wildcard *.c libs/???*/*.c)
OBJ = $(subst .c,.o,$(SOURCES))

all:	demo1

demo1:  $(OBJ)	globals.h
	${CC} ${CFLAGS} ${LDFLAGS} -o battletaxi battletaxi.c drawings.o starfield.o fonts.o sound.o taxi.o menu.o level.o man.o bubble.o hud.o game.o weapons.o highscore.o input.o background.o dirs.o splash.o voxel.o sky.o messages.o libs/sfont/SFont.o libs/confuse/confuse.o libs/confuse/lexer.o

%.o: %.c %.h globals.h
	${CC} ${CFLAGS} -c $< -o $@
 
clean:
	rm -f *.o
	rm -f battletaxi
	rm -f *~
	rm -f gmon.out
	rm -f *.greg
	rm -f libs/*/*.o

