#ifndef BUBBLE_H
#define BUBBLE_H

typedef struct S_BUBBLE{
	int x,y;
	int bubblex,bubbley;
	unsigned int enddisplay;
	int visible;
	int textwidth;
	char *text;
	FontFactory *ff;
}Bubble;

Bubble *bubbleInit (FontFactory *ff);
void bubbleStart(Bubble *abubble, int ax, int ay, char *name, int displaytime);
void bubbleDraw(Bubble *abubble, SDL_Surface *ascreen);
void bubbleExit(Bubble *abubble);

// internal stuff

#endif
