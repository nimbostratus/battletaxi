#ifndef INPUT_H
#define INPUT_H

#define INPUT_STAT_NORMAL	0x00
#define INPUT_STAT_VISIBLE	0x01

typedef void (inputcallb_t) (void *userdata, char *input);

typedef struct S_INPUTBOX{
	char *refname;
	char *prompt;
	char *input;
	
	inputcallb_t *action;
	void *userdata;
	
	struct S_INPUTBOX *next;
}InputBox;

typedef struct S_INPUTFACTORY{
	int status;
	int acceptinputat;
	Uint8 keycopy[322];
	InputBox *boxes;
	InputBox *aktbox;
}InputFactory;

InputFactory *inputInit ();
void inputDraw (SDL_Surface *asurface, InputFactory *ainput, FontFactory *afont, int afontnum, Uint8 *akeystroke);
void inputExit(InputFactory *ainput);

void inputInsBox(InputFactory *ainput, char *arefname, char *prompt, inputcallb_t *aaction, void *auserdata);
void inputShowBox(InputFactory *ainput, char *arefname);

#endif
