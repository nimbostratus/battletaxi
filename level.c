#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include "dirs.h"
#include <string.h>
#include "messages.h"
#include <confuse.h>
#include "background.h"
#include "includes.h"

LevelFactory *levelInit(char *apath)
{

	assert (apath);

	Level *runner;
	char *timg=NULL;
	char *tcolmap=NULL;
	char *tname=NULL;
	char *tbgeffects=NULL;
	double tgravity=0.0;
	double tliftoffspeed=0.0;
	char thisfile[NAME_MAX+1];
	int matchcolor, i;
	int parseret=0;
	Point p;

	cfg_opt_t opts[] = {
		CFG_SIMPLE_STR("name", &tname),
		CFG_SIMPLE_STR("imagemap", &timg),
		CFG_SIMPLE_STR("collisionmap", &tcolmap),
		CFG_SIMPLE_STR("bgeffects", &tbgeffects),
		CFG_SIMPLE_FLOAT("gravity", &tgravity),
		CFG_SIMPLE_FLOAT("liftoffspeed", &tliftoffspeed),
		CFG_END()
	};
	cfg_t *cfg;
	
	char **mapfiles;
	char *tmpstr;
	char displaystr[200];
	
	mapfiles = DirGetFiles (apath, ".conf");
	
	cfg = cfg_init(opts, 0);
	
	LevelFactory *ret=(LevelFactory*)malloc(sizeof(LevelFactory));
	runner=ret->levellist=ret->aktlevel=(Level*)malloc(sizeof(Level));
	ret->aktlevelnum=0;
	
	ret->maxlevel=0;
	
	for (i=0; mapfiles[i] != NULL; ++i){
		sprintf (thisfile, "%s/%s", apath, mapfiles[i]);
		parseret = cfg_parse(cfg, thisfile);

		runner->next=(Level*)malloc(sizeof(Level));
		
		runner->name = (char*)malloc(strlen (tname)+1);
		strcpy (runner->name,tname);

		snprintf (displaystr, 199, "Loading map %s...", tname);
		splashStep (displaystr);

		runner->gravity = tgravity;
		runner->liftoffspeed = tliftoffspeed;

		runner->numbgeffects = 0;
		runner->bgeffects = (char**) malloc (sizeof(char*));

		tmpstr = strtok (tbgeffects, ",");
		while ( tmpstr != NULL  )
		{
			runner->bgeffects = (char **)realloc (runner->bgeffects, sizeof(char*) * (runner->numbgeffects +2));
			runner->bgeffects[runner->numbgeffects] = (char *) malloc (strlen (tmpstr) +1);

			strcpy (runner->bgeffects[runner->numbgeffects], tmpstr);
			runner->numbgeffects++;
			tmpstr = strtok (NULL, ",");
		}
		runner->bgeffects[runner->numbgeffects] = NULL;

		sprintf(thisfile, "%s/%s", apath, tcolmap);
		runner->colmap=imageloadalpha (thisfile);
		if(runner->colmap==NULL){
			runner->next=NULL;
			return ret;
		}
		sprintf(thisfile, "%s/%s", apath, timg);
		runner->img=imageloadalpha(thisfile);
		
		for(matchcolor=0xff0000;drawGetFirstColor(&p, runner->colmap, matchcolor);matchcolor+=0x0100);
		runner->plattformcount=((matchcolor&0xff00)>>8);
	

		ret->maxlevel++;
		runner=runner->next;
	}
	runner->next=NULL;
	
	return ret;
}

void levelSwitch(LevelFactory *alevel, BackgroundFactory *abackground, int number)
{
	assert (alevel);
	assert (abackground);
	Level *runner=alevel->levellist;
	Background *tmpbg;
	int i;
	
	alevel->aktlevelnum=number;
	for(i=0;i<number&&runner->next!=NULL;i++,runner=runner->next);
	if(runner->next!=NULL)
	{
		alevel->aktlevel=runner;

		// Enable all Backgrounds used by this level
		backgroundDisableAll (abackground);
		for (i=0; i<alevel->aktlevel->numbgeffects; ++i)
		{
			tmpbg = getBackgroundByName (abackground, alevel->aktlevel->bgeffects[i]);
			if (!tmpbg)
				fprintf (stderr, "warning: background %s not found - disabled\n", alevel->aktlevel->bgeffects[i]);
			else
				tmpbg->active = 1;
		}
		
		messageThrow (alevel->aktlevel->name);
	}
}

void levelDraw (SDL_Surface *ascreen, LevelFactory *alevel)
{
	SDL_Rect rcsrc, rcdst;
	
	rcsrc.x=rcdst.x=0; 	rcsrc.y=rcdst.y=0;
	rcdst.w=rcsrc.w=ascreen->w;
	rcdst.h=rcsrc.h=ascreen->w;
	
	SDL_BlitSurface(alevel->aktlevel->img, &rcsrc, ascreen, &rcdst);
}

void levelExit(LevelFactory *alevel)
{
	Level *runner=alevel->levellist, *tmp;
	
	while(runner->next!=NULL){
		tmp=runner->next;
		SDL_FreeSurface(runner->img);
		SDL_FreeSurface(runner->colmap);
		if (runner->name != NULL)
			free (runner->name);
		free(runner);
		runner=tmp;
	}		
	free(alevel);
}

int levelGetRandPlattform(LevelFactory *alevels)
{
	return rand()%(alevels->aktlevel->plattformcount);
}
