#ifndef TAXI_H
#define TAXI_H

#define IDX_COUNT		13

#define TAXI_STAT_NORMAL	0x000
#define TAXI_STAT_LANDED	0x001
#define TAXI_STAT_JUSTLANDED	0x002
#define TAXI_STAT_HASPASSENGER	0x004
#define TAXI_STAT_GEAROUT	0x008
#define TAXI_STAT_CRASHED	0x010
#define TAXI_STAT_DIRLEFT	0x020
#define TAXI_STAT_DIRRIGHT	0x040
#define TAXI_STAT_JUSTCRASHED	0x080

#define IDX_TORIGHTCOLLAPSED 	0
#define IDX_TORIGHTEXPANDED 	1
#define IDX_TOLEFTCOLLAPSED 	2
#define IDX_TOLEFTEXPANDED 	3

#define IDX_FIRENORTHTORIGHT	4
#define IDX_FIRENORTHTOLEFT	5
#define IDX_FIREEAST		6
#define IDX_FIRESOUTH		7
#define IDX_FIREWEST		8
#define IDX_CRASHTOLEFT		9
#define IDX_CRASHTORIGHT	10
#define IDX_PASSENGERRIGHT	11
#define IDX_PASSENGERLEFT	12

#define DIR_LEFT		0
#define DIR_RIGHT		1

typedef struct S_TAXIFACTORY{
	SDL_Surface *images[IDX_COUNT];
	double x,y;
	double dx,dy;
	int status;
	int weapon;
	unsigned int lastspacepress;
}TaxiFactory;

TaxiFactory *taxiInit (char *fnamestart, SDL_Surface *colmap);
void taxiDraw (SDL_Surface *ascreen, TaxiFactory *ataxi, Uint8 *keystroke, SDL_Surface *colmap);
void taxiExit(TaxiFactory *ataxi);
void taxiSetAlive (TaxiFactory *ataxi, SDL_Surface *colmap);

// internal stuff

void taxiDrawFrame(SDL_Surface *ascreen, TaxiFactory *ataxi, int whichimg);
void initXyOnLevel(TaxiFactory *ataxi, SDL_Surface *colmap);

#endif
