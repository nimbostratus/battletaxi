#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include "includes.h"


Man *manInit (char *fnamestart, FontFactory *ff)
{
	int i;
	char *fname=(char*)malloc(strlen(fnamestart)+10);
	Man *ret=(Man *)malloc(sizeof(Man));
	
	for(i=0;i<M_IDX_COUNT;i++){
		sprintf(fname,"%s-%d.png",fnamestart,i);
		ret->images[i]=imageloadalpha (fname);
	}
	free(fname);
	
	ret->x=0;
	ret->y=0;
	ret->dx=0;
	ret->status=MAN_STAT_NORMAL;
	ret->plattform=0;
	ret->walkendx=0;
	ret->nextmanticks=0;
	ret->bubble=bubbleInit(ff);
	
	return ret;		
}

void manStart(Man *aman, SDL_Surface *colmap, int plattform)
{
	// replace this with a faster grabpixelfirstcolor(SDL_Surface *adst, UInt32 searchcolor);
	Point p;
	int x;
	int matchcolor=(0xff0000|(plattform<<8));
	
	if(!drawGetFirstColor(&p, colmap, matchcolor)){
		DEBUG("Error, plattform id not found:(hex)");
		DEBUGx(matchcolor);
	}
	
	for(x=p.x;(grabpixel(colmap, x, p.y)&0xFFFFFF)==matchcolor;x++);
	
	aman->x=x-aman->images[0]->w-1;
	aman->y=p.y-aman->images[0]->h-1;
	aman->dx=0;
	aman->status=false;
	aman->plattform=plattform;
	aman->drawframe=M_IDX_WAVEONE;
	aman->nextmanticks=0;
	aman->walkendx=p.x+40;
	aman->tickclock=SDL_GetTicks();
	
	bubbleStart(aman->bubble, x, p.y, "Hello Taxi", DISPLAYTIME);
	aman->status|=MAN_STAT_VISIBLE;
}

void manDraw(SDL_Surface *ascreen, Man *aman, TaxiFactory *ataxi, LevelFactory *alevels, HudFactory *ahud)
{
	SDL_Rect rcsrc, rcdst;
		
	rcsrc.x=0;		rcsrc.y=0;
	rcdst.x=aman->x;	rcdst.y=aman->y;
	rcdst.w=rcsrc.w=aman->images[aman->drawframe]->w;
	rcdst.h=rcsrc.h=aman->images[aman->drawframe]->w;
	if(aman->status&MAN_STAT_VISIBLE) 
	        SDL_BlitSurface(aman->images[aman->drawframe], &rcsrc, ascreen, &rcdst);
}

void manExit(Man *aman)
{
	int i;
	
	for(i=0;i<M_IDX_COUNT;i++)
		SDL_FreeSurface(aman->images[i]);
	bubbleExit(aman->bubble);
	free(aman);
}

