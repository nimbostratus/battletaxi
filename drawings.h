#ifndef DRAWINGS_H
#define DRAWINGS_H

#include "globals.h"
#include <SDL.h>
#include <SDL_image.h>


SDL_Surface* initsdl (const int aw, const int ah, const int ad);
/*
	Initializes the needed sdl modules, sets the videomode to the
	given width, height an depth
	Arguments: 	aw - Width of surface
			ah - Height of surface
			ad - Depth of Surface (8,16,32) in bpp
	Returns		Created Surface if successful
			nothing if it fails -> abort


*/

void shutdownsdl (SDL_Surface* ascreen);
/*
	Sets free the Screen and shuts down sdl
	Arguments:	ascreen - Surface to free()
	returns		--
*/


int begindrawing (SDL_Surface* ascreen);
/*	
	Prepares a surface for drawing single Pixels, Lines and Boxes.
	Due to this, the Surface will be locked, if it must be locked. 
	Arguments:	ascreen - the screen to lock, may be the display surface
	Returns		1 if successful, 0 if it fails
*/

void switchfullscreen (void);
	
void blanksurface(SDL_Surface *ascreen);

void enddrawing (SDL_Surface* ascreen);
/*
	Unlockes the given Surface and Calls a UpdateRect(). The UpdateRect will
	update the complete Screen. Maybe it would sometimes be faster to 
	update not more than the Pixel drawn.
	Arguments	ascreen - Screen to unlock, may be the display surface
	Returns		---
*/

int drawpixel (SDL_Surface* ascreen, const int ax, const int ay, const int ar, const int ag, const int ab);
/*
	Draws a Pixel on the wanted position at the Surface. If it's outside
	the valid area, the pixel wouldn't be set. 
	Arguments:	ascreen  - Screen to draw on
			ax,ay    - x,y Position
			ar,ag,ab - RGB Color Value of the Pixel
	Returns		1 if pixel was set
			0 if pixel was outside the valid area
	Prints out a Warning to stderr
*/

void drawline (SDL_Surface *ascreen, int x, int y, int x2, int y2, int ar, int ag, int ab);
/*
	Draws a Line between (x,y)(x2,y2) with the given Color. 
	Returns		---
*/


Uint32 grabpixel (SDL_Surface *ascreen, int ax, int ay);

int drawGetFirstColor(Point *ret, SDL_Surface *acolmap, int matchcolor);

SDL_Surface *imageloadalpha (char *afile);

#endif
