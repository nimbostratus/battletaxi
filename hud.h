#ifndef HUD_H
#define HUD_H

typedef struct S_HUDFACTORY{
	SDL_Surface *surface;
	SDL_Surface *background;
	int	alpha;
	int	visible;
	int	score;
	int	cash;
	int	frags;
	int	ranking;
	int	fuel;
	int	fading;	// 1 in, -1 out, 0 no fading
	int	y;
	unsigned int	laststatechange;
} HudFactory;


HudFactory *hudInit (char *aimage);
void hudReset(HudFactory *ahud);
void hudExit (HudFactory *ahud);

void hudSetScore (HudFactory *ahud, int ascore);
void hudAddScore (HudFactory *ahud, int aadd);

void hudDraw(SDL_Surface *ascreen, FontFactory *afont, int afontnum, HudFactory *ahud);
void hudShow(HudFactory *ahud);
void hudHide(HudFactory *ahud);
void hudToggle(HudFactory *ahud);


#endif
