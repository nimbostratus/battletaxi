#include <malloc.h>
#include <stdlib.h>
#include "includes.h"

// update stuff - main game-operations!
void gameUpdate (Game *g)
{
	int ticks=SDL_GetTicks()-g->lastkeyticks;
	g->lastkeyticks=SDL_GetTicks();
	
	gameUpdateKeyboard(g);
	
	if(g->menuactive || g->input->status&INPUT_STAT_VISIBLE){
		g->taxi->status&=~TAXI_STAT_JUSTLANDED;
		g->taxi->status&=~TAXI_STAT_JUSTCRASHED;
		return;
	}
		
	gameUpdateWeapon(g);
	gameUpdateTaxi(g, ticks);
	gameUpdateMan(g, ticks);
}

void gameUpdateWeapon(Game *g)
{
	Shot *runner=g->weapons->shots, *before, *next;
	int scoreset=false;
	
	while(runner->next!=NULL){
		if (runner->weapon->type == WEAPON_TYPE_LASER)
			runner->x+=runner->dx;
		else if (runner->weapon->type == WEAPON_TYPE_BOMB)
			runner->y+=runner->dy;
		next=runner->next;
		
		//delete element if collides or is offscreen
		if(gameUpdateWeaponDoesCollied(g, runner) || (runner->x<-runner->weapon->shotimage->w || runner->x>MAXX)){
			// catch first
			if(runner==g->weapons->shots)
				g->weapons->shots=runner->next;
			// handle normal elemet
			else{
				for(before=g->weapons->shots;before->next!=runner;before=before->next);
				before->next=runner->next;
			}
			free(runner);
			runner=NULL;
		}
		if(runner!=NULL && gameUpdateWeaponHitsPassenger(g, runner)){
			if(!scoreset){
				hudAddScore(g->hud, 10);
				scoreset=true;
			}
			manStart(g->man, g->levels->aktlevel->colmap, levelGetRandPlattform(g->levels));
		}
			
		runner=next;
	}
}

int  gameUpdateWeaponHitsPassenger(Game *g, Shot *s)
{
	Man *m=g->man;
	int w=m->images[0]->w, h=m->images[0]->h;
	
	if(s->x>m->x && s->x<m->x+w && s->y>m->y && s->y<m->y+h)
		return true;
		
	return false;
}


int  gameUpdateWeaponDoesCollied(Game *g, Shot *s)
{
	#define SHOOTCLIPRECT 5
	int y,h=s->weapon->shotimage->h;
	
	// left&right line
	for(y=s->y;y<s->y+h;y++){
		if((grabpixel(g->levels->aktlevel->colmap, s->x, y)&0xFF)==0xFF)
			return true;
		if((grabpixel(g->levels->aktlevel->colmap, s->x+h, y)&0xFF)==0xFF)
			return true;
	}
	
	return false;
}

void gameUpdateTaxi(Game *g, int ticks)
{
	gameUpdateTaxiKeyboard(g, ticks);
//	gameUpdateTaxiNetwork(g);
}

void gameUpdateTaxiKeyboard(Game *g, int ticks)
{
	int lrpressed=0, startx, delta;	
	if(g->keystroke[SDLK_LCTRL] && !(g->taxi->status&TAXI_STAT_LANDED)){
		if (g->weapons->weapons[g->taxi->weapon]->type == WEAPON_TYPE_LASER)
		{
			if(g->taxi->status&TAXI_STAT_DIRRIGHT){
				startx=g->taxi->x+g->taxi->images[0]->w-10;
				delta=g->weapons->weapons[g->taxi->weapon]->speed;
			}else{
				startx=g->taxi->x;
				delta=-(g->weapons->weapons[g->taxi->weapon]->speed);
			}
		}
		else if (g->weapons->weapons[g->taxi->weapon]->type == WEAPON_TYPE_BOMB)
		{
				startx=g->taxi->x + (g->taxi->images[0]->w/2);
				delta=g->weapons->weapons[g->taxi->weapon]->speed;
		}
		if(SDL_GetTicks()>g->nextshot){
			weaponNewShot(g->weapons, startx, g->taxi->y+g->taxi->images[0]->h/2, delta, g->taxi->weapon);
			g->nextshot=SDL_GetTicks()+FIRERATE; //TODO: Firerate -> weapon.conf
		}
	}
	
	if(g->keystroke[SDLK_UP]){
		g->taxi->dy-=SPEED*ticks;
	}
	
	if(g->keystroke[SDLK_LEFT]&&!(g->taxi->status&TAXI_STAT_GEAROUT)){
		g->taxi->status|=TAXI_STAT_DIRLEFT;
		g->taxi->status&=~TAXI_STAT_DIRRIGHT;
		g->taxi->dx-=SPEED*ticks;
		lrpressed=1;
	}
	
	if(g->keystroke[SDLK_DOWN]){
		g->taxi->dy+=SPEED*ticks;
	}
	// the quick and dirty weapon chooser
	if(g->keystroke[SDLK_1]){
		g->taxi->weapon=0;
		messageThrow(g->weapons->weapons[0]->name);
	}
	if(g->keystroke[SDLK_2]){
		g->taxi->weapon=1;
		messageThrow(g->weapons->weapons[1]->name);
	}
	if(g->keystroke[SDLK_3]){
		g->taxi->weapon=2;
		messageThrow(g->weapons->weapons[2]->name);
	}
	if(g->keystroke[SDLK_4]){
		g->taxi->weapon=3;
		messageThrow(g->weapons->weapons[3]->name);
	}
	if(g->keystroke[SDLK_RIGHT]&&!(g->taxi->status&TAXI_STAT_GEAROUT)){
		g->taxi->status&=~TAXI_STAT_DIRLEFT;
		g->taxi->status|=TAXI_STAT_DIRRIGHT;
		g->taxi->dx+=SPEED*ticks;
		lrpressed=1;
	}
	if(g->keystroke[SDLK_SPACE]){
		g->taxi->status&=~TAXI_STAT_LANDED;
		if(SDL_GetTicks() - g->taxi->lastspacepress > 250){
			if(g->taxi->status&TAXI_STAT_GEAROUT)
				g->taxi->status&=~TAXI_STAT_GEAROUT;
			else
				g->taxi->status|=TAXI_STAT_GEAROUT;
			g->taxi->lastspacepress = SDL_GetTicks();
		}
	}

	if(!lrpressed){
		if(abs(g->taxi->dx*ticks)>SLOWDOWN*ticks)
			g->taxi->dx+= g->taxi->dx>0 ? -SLOWDOWN*ticks : +SLOWDOWN*ticks;
		else g->taxi->dx=0;
	}

	if(g->taxi->status&TAXI_STAT_LANDED||g->taxi->status&TAXI_STAT_CRASHED) return;
	
	g->taxi->x+=g->taxi->dx*ticks;
	g->taxi->y+=g->taxi->dy*ticks;
	g->taxi->dy+=g->levels->aktlevel->gravity*ticks;
	
	g->taxi->status&=~TAXI_STAT_JUSTLANDED;
	g->taxi->status&=~TAXI_STAT_JUSTCRASHED;
	gameUpdateTaxiCheckCrashed(g->taxi, g->levels->aktlevel->colmap);
}

void gameUpdateTaxiCheckCrashed(TaxiFactory *ataxi, SDL_Surface *colmap)
{
	int x,y,downy;

	if((ataxi->status&TAXI_STAT_CRASHED)||(ataxi->status&TAXI_STAT_LANDED))
		return;
	
	if(ataxi->status&TAXI_STAT_GEAROUT)	downy=35;
	else					downy=29;
	
	for(x=10;x<81;x++){
		// just landed
		if(ataxi->status&TAXI_STAT_GEAROUT&&(grabpixel(colmap, ataxi->x+x, ataxi->y+downy)&0xFF0000)==0xFF0000){
			if(abs(ataxi->dx)>LANDSPEED || abs(ataxi->dy>LANDSPEED)) {
				ataxi->status|=TAXI_STAT_CRASHED;
				ataxi->status|=TAXI_STAT_JUSTCRASHED;
			}else{
				ataxi->status|=TAXI_STAT_LANDED;
				ataxi->status|=TAXI_STAT_JUSTLANDED;
			}
			ataxi->dx=0;
			ataxi->dy=0;
			return;
		}
		if((grabpixel(colmap, ataxi->x+x, ataxi->y+3)&0x0000ff)==0x0000ff){
			ataxi->status|=TAXI_STAT_CRASHED;
			ataxi->status|=TAXI_STAT_JUSTCRASHED;
		}
		if((grabpixel(colmap, ataxi->x+x, ataxi->y+downy)&0x0000ff)==0x0000ff){
			ataxi->status|=TAXI_STAT_CRASHED;
			ataxi->status|=TAXI_STAT_JUSTCRASHED;
		}
	}
	for(y=3;y<downy;y++){
		if((grabpixel(colmap, ataxi->x+10, ataxi->y+y)&0x0000ff)==0x0000ff){
			ataxi->status|=TAXI_STAT_CRASHED;
			ataxi->status|=TAXI_STAT_JUSTCRASHED;
		}
		if((grabpixel(colmap, ataxi->x+81, ataxi->y+y)&0x0000ff)==0x0000ff){
			ataxi->status|=TAXI_STAT_CRASHED;
			ataxi->status|=TAXI_STAT_JUSTCRASHED;
		}
	}

}

void gameUpdateMan(Game *g, int ticks)
{
	// animate sprite all 250 msecs
	if(SDL_GetTicks()-g->man->tickclock>250){
		g->man->tickclock=SDL_GetTicks();
		if(g->man->status&MAN_STAT_WALKING)
			g->man->drawframe = g->man->drawframe==M_IDX_WALKLEFTONE ? M_IDX_WALKLEFTTWO : M_IDX_WALKLEFTONE;
		else{
			g->man->drawframe++;
			if(g->man->drawframe>M_IDX_WAVEFOUR)
				g->man->drawframe=M_IDX_WAVEONE;
		}
	}
	
	// reinit man on timer
	if(g->man->nextmanticks && (SDL_GetTicks() > g->man->nextmanticks)){
		g->man->nextmanticks=0;
		manStart(g->man, g->levels->aktlevel->colmap, levelGetRandPlattform(g->levels));
	}
	
	// if just crashed, update highscore
	if((g->taxi->status&TAXI_STAT_JUSTCRASHED) &&highscoreIsNew(g->highscore, g->hud->score)){
		inputShowBox(g->input, "inputName");
	}
	
	// do things when taxi is landed
	if(g->taxi->status&TAXI_STAT_LANDED&& !(g->taxi->status&TAXI_STAT_CRASHED))
		gameUpdateManLanded(g);
	
	// end of walking ?
	if(g->man->x<g->man->walkendx){
		g->man->status&=~MAN_STAT_VISIBLE;
		g->man->dx=0;
	}
	g->man->x+=g->man->dx*ticks;
}

void gameUpdateManLanded(Game *g)
{
	unsigned int pixel=grabpixel(g->levels->aktlevel->colmap, g->taxi->x+g->taxi->images[0]->w/2, g->taxi->y+g->taxi->images[0]->h);
	int plattform=(pixel&0x00FF00)>>8, bubx, buby, oldplattform;
	
	char plattstr[40];
	
	// wrong plattform! chant at player! and return
	if(plattform!=g->man->plattform){
		if(!(g->taxi->status&TAXI_STAT_JUSTLANDED)) return;
		bubx=g->man->x; buby=g->man->y;
		if(g->taxi->status&TAXI_STAT_HASPASSENGER){
			bubx=g->taxi->x+g->taxi->images[0]->w/2;
			buby=g->taxi->y;
			sprintf(plattstr, "Idiot! Bring me to %d!", g->man->plattform+1);
		}else{
			sprintf(plattstr, "Hey, i am here!!!! Punk!");
			g->man->status|=MAN_STAT_VISIBLE;
		}
		bubbleStart(g->man->bubble, bubx, buby, plattstr, DISPLAYTIME);
		return;
	}
	
	// taxi has JUST landed on right plattform! start walking and update stuff
	if(g->taxi->status&TAXI_STAT_JUSTLANDED){
		g->man->status|=MAN_STAT_WALKING;
		g->man->walkendx=g->taxi->x+g->taxi->images[0]->w/2;
	}
	
	// just landed on mans target plattform with passenger! put him out and start walking
	if(g->taxi->status&TAXI_STAT_HASPASSENGER){
		hudAddScore(g->hud, 15);
		g->man->x=g->taxi->x+g->taxi->images[0]->w/2;
		g->man->y=g->taxi->y;
		g->man->walkendx=g->taxi->x-30;
		g->man->status|=MAN_STAT_WALKING;
		g->man->status|=MAN_STAT_VISIBLE;
		
		bubbleStart(g->man->bubble, g->man->x, g->man->y, "Thanks!", DISPLAYTIME);
		
//		g->taxi->status&=~TAXI_STAT_LANDED;
//		g->taxi->status&=~TAXI_STAT_GEAROUT;
		g->taxi->status&=~TAXI_STAT_HASPASSENGER;
		g->taxi->dy=g->levels->aktlevel->liftoffspeed;
		g->man->nextmanticks=SDL_GetTicks()+CUSTOMERTIMEOUT;
	}
	
	// passanger has arived at taxi - we now start the taxi again
	if(g->man->x<g->man->walkendx&&!g->man->nextmanticks){
		hudAddScore(g->hud, 5);
		g->taxi->status|=TAXI_STAT_HASPASSENGER;
		g->taxi->dy=g->levels->aktlevel->liftoffspeed;
		oldplattform=g->man->plattform;
		while((g->man->plattform=levelGetRandPlattform(g->levels))==oldplattform);
		sprintf(plattstr, "Take me to Plattform %d", g->man->plattform+1);
		bubbleStart(g->man->bubble, g->man->x, g->man->y, plattstr, DISPLAYTIME);
	}
	
	g->man->dx=MANSPEED;	
}

void gameUpdateKeyboard(Game *g)
{
		g->keystroke = SDL_GetKeyState(NULL);
		if (g->keystroke[SDLK_F4])
			g->quit=1;
		if (g->keystroke[SDLK_TAB])
			hudToggle (g->hud);
		if (g->keystroke[SDLK_DOWN])
			if (g->menuactive) mnuProcess(g->mainmenu, MENUACTION_NEXT);
		if (g->keystroke[SDLK_UP])
			if (g->menuactive) mnuProcess(g->mainmenu, MENUACTION_PREV);
		if (g->keystroke[SDLK_LEFT])
			if (g->menuactive) mnuProcess(g->mainmenu, MENUACTION_DECREASE);
		if (g->keystroke[SDLK_RIGHT])
			if (g->menuactive) mnuProcess(g->mainmenu, MENUACTION_INCREASE);
		if (g->keystroke[SDLK_RETURN])
			if (g->menuactive) mnuProcess (g->mainmenu, MENUACTION_ACTIVATE);
		if (g->keystroke[SDLK_ESCAPE]){
			if(!(g->nextescape<SDL_GetTicks()))
				return;
			g->nextescape=SDL_GetTicks()+500;
			if(g->highscoreactive==true)
				g->highscoreactive=false;
			else{
				if(g->menuactive==false)
					g->menuactive = true;
				else
					g->menuactive = false;
			}
		}

}

// init and exit ...
Game *gameInit ()
{
	char *fontlist[]={"images/font0.jpg", "images/font1.jpg", "images/font2.png", NULL};
	Game *ret=(Game *)malloc(sizeof(Game));

	ret->framecounter=0;
	ret->nextescape=0;
	ret->pause=false;
	ret->fps=0;
	ret->quit=false;
	ret->menuactive=true;
	ret->nextshot=0;
	ret->highscoreactive=false;
	ret->lastkeyticks=SDL_GetTicks();
		
	ret->screen = initsdl (MAXX, MAXY, BPP);

	ret->fontfac=bmpFactoryInit(fontlist);
	ret->messages=messageInit(500, ret->fontfac);

	splashInit("images/splash.png", "images/splashstep.png", 10);
	splashStep("Loading game maps...");
	
	ret->levels=levelInit("maps");
	splashStep("Initializing background effects...");
	ret->bg=backgroundInit();
	splashStep("Loading weapons...");
	ret->weapons=weaponInit("weapons");
	splashStep("Loading man data...");
	ret->man=manInit("images/man1",ret->fontfac);
	splashStep("Loading taxi data...");
	ret->taxi=taxiInit("images/taxi1", ret->levels->aktlevel->colmap);
	splashStep("Loading menu data...");
	ret->mainmenu=mnuInit ("images/selectorr.png", "images/selectorl.png", ret->fontfac, ret->screen);
	splashStep("Loading hud...");
	ret->hud=hudInit ("images/hud.png");
	splashStep("Loading highscore...");
	ret->highscore=highscoreInit();
	ret->input=inputInit();
	
	inputInsBox(ret->input, "inputName", "Name: ", inputCallbName, ret);
	inputInsBox(ret->input, "inputHost", "Connect to: ", inputCallbHost, ret);

	manStart(ret->man, ret->levels->aktlevel->colmap, 1);
	
	gameInitMenu(ret);

splashDone();

	return ret;
}

void gameExit(Game *g)
{
	mnuExit (g->mainmenu);
	hudExit (g->hud);
	taxiExit(g->taxi);
	manExit(g->man);
	levelExit(g->levels);
	bmpFactoryExit(g->fontfac);
	messageExit(g->messages);
	backgroundExit(g->bg);
	weaponExit(g->weapons);
	highscoreExit(g->highscore);
	inputExit(g->input);

	shutdownsdl (g->screen);
	free(g);
}

// internal stuff
void gameInitMenu(Game *g)
{	
	MenuFactory *mainmenu=g->mainmenu;
	char *fullscreenlist[]={"Windowed", "Fullscreen", NULL};
	
	mnuAddMenuItem (mainmenu, "mainStartGame", "Start Game");
	mnuAddMenuItem (mainmenu, "mainConnect", "Connect to Server");
	mnuAddListItem (mainmenu, "mainFullscreen", "Window Mode (( %s ))", fullscreenlist, 0);
 	mnuAddNumericItem (mainmenu, "mainLevel", "Select Level (( %d ))", 1, g->levels->maxlevel, 1);
	mnuAddMenuItem (mainmenu, "mainHighscore", "Highscores");
	mnuAddMenuItem (mainmenu, "mainQuit", "Quit Game");
	
	mnuAttachEvent (mainmenu, "mainStartGame", menuCallbNew, g);
	mnuAttachEvent (mainmenu, "mainConnect", menuCallbConnect, g);
	mnuAttachEvent (mainmenu, "mainHighscore", menuCallbHighscore, g);
	mnuAttachEvent (mainmenu, "mainQuit", menuCallbQuit, g);
	mnuAttachEvent (mainmenu, "mainLevel", menuCallbLevel, g);
	mnuAttachEvent (mainmenu, "mainFullscreen", menuCallbFull, NULL);
 	
	mnuSetActive (mainmenu, "mainStartGame");
}

// callback functions
void menuCallbQuit (Menu *amenu, void *adata)
{
	Game *g = (Game*)adata;
	g->quit=true;
}

void menuCallbConnect (Menu *amenu, void *adata)
{
	Game *g = (Game*)adata;
	g->menuactive=false;
	inputShowBox(g->input, "inputHost");
}

void menuCallbNew (Menu *amenu, void *adata)
{
	Game *g = (Game*)adata;
	
	g->lastkeyticks=SDL_GetTicks();
	taxiSetAlive (g->taxi, g->levels->aktlevel->colmap);
	manStart(g->man, g->levels->aktlevel->colmap, levelGetRandPlattform(g->levels));
	hudReset(g->hud);
	weaponDeleteShots(g->weapons);
	g->menuactive = false;
}

void menuCallbLevel (Menu *amenu, void *adata)
{
	Game *g = (Game*)adata;
	levelSwitch (g->levels, g->bg, g->mainmenu->active->value-1);
}

void menuCallbHighscore (Menu *amenu, void *adata)
{
	Game *g = (Game*)adata;
	g->highscoreactive=true;
}

void menuCallbFull (Menu *amenu, void *adata)
{
	switchfullscreen();
}

void inputCallbName(void *vgame, char *ainput)
{
	Game *g=(Game*)vgame;
	
	g->input->status&=~INPUT_STAT_VISIBLE;
	g->showhighscoretillticks=SDL_GetTicks()+3000;
	highscoreInsNewScore(g->highscore, g->hud->score, ainput, g->levels->aktlevelnum+1);
}

void inputCallbHost(void *vgame, char *ainput)
{
	Game *g=(Game*)vgame;
	
	g->input->status&=~INPUT_STAT_VISIBLE;
	g->menuactive=false;
}
